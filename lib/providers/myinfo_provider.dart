import 'package:flutter/cupertino.dart';

class MyInfoProvider with ChangeNotifier{

  bool _alarmCheck = true;
  String _voiceName;
  String _voiceImg;
  List<String> _alarmDays = ['Day'];
  String _languageImg ;
  String _languageName;
  int _languageIndex;
  int _voiceIndex = 0;
  String _languageNation;
  String _alarmDate;
  DateTime _alarmDateOrigin;
  List<bool> _alarmDaysCheck = [false, false, false, false, false, false, false,];


  bool get alarmCheck => _alarmCheck;
  String get voiceName => _voiceName;
  String get voiceImg => _voiceImg;
  List<String> get alarmDays => _alarmDays;
  String get languageImg => _languageImg;
  String get languageName => _languageName;
  int get languageIndex => _languageIndex;
  int get voiceIndex => _voiceIndex;
  String get languageNation => _languageNation;
  String get alarmDate => _alarmDate;
  DateTime get alarmDateOrigin => _alarmDateOrigin;
  List<bool> get alarmDaysCheck => _alarmDaysCheck;

  //alarm
  void setAlarmCheck(bool check){
    _alarmCheck = check;
    notifyListeners();
  }

  void setAlarmDate(String date){
    _alarmDate = date;
    notifyListeners();
  }

  void setAlarmDateOrigin(DateTime date){
    _alarmDateOrigin = date;
    notifyListeners();
  }

  void setAlarmDaysCheck(int idx){
    _alarmDaysCheck[idx] = !_alarmDaysCheck[idx];
    notifyListeners();
  }

  void loadAlarmDaysCheck(List<bool> value){
    _alarmDaysCheck = value;
    // notifyListeners();
  }

  void setAlarmDays(String day){
    if(_alarmDays.contains(day) == false){
      _alarmDays.add(day);
    }
    notifyListeners();
  }

  void popAlarmDays(String day){
    _alarmDays.remove(day);
    notifyListeners();
  }

  //lang
  void setLanguageName(String name){
    _languageName = name;
    notifyListeners();
  }

  void setLanguageImg(String name){
    _languageImg = name;
    notifyListeners();
  }

  void setLanguageIndex(int idx){
    _languageIndex = idx;
    notifyListeners();
  }

  void setLanguageNation(String name){
    if (name == 'English'){
      _languageNation = 'U.S.A';
    }else if(name =='Indonesian'){
      _languageNation = 'Indonesia';
    }else{
      _languageNation = 'Vietnam';
    }
    notifyListeners();
  }

  //voice
  void setVoiceName(String name){
    _voiceName = name;
    notifyListeners();
  }

  void setVoiceImg(String img){
    _voiceImg = img;
    notifyListeners();
  }

  void setVoiceIndex(int idx){
    _voiceIndex = idx;
    notifyListeners();
  }

}