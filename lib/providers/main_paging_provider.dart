import 'package:flutter/material.dart';

class MainPagingProvider with ChangeNotifier{
  int _currentPage;
  PageController _pageController;

  int get currentPage => _currentPage;
  PageController get pageController => _pageController;

  void setCurrentPage(int currentPage){
    _currentPage = currentPage;
  }

  void setPageController(PageController pageController){
    _pageController = pageController;
  }

  void jumpToPageByName(String name){
    if(name == 'Home') {
      _pageController.jumpToPage(0);
      _currentPage = 0;
    }else if(name == 'Course'){
      _pageController.jumpToPage(1);
      _currentPage = 1;
    }else if(name == 'Tutor'){
      _pageController.jumpToPage(2);
      _currentPage = 2;
    }else if(name == 'MyInfo'){
      _pageController.jumpToPage(3);
      _currentPage = 3;
    }else{
      _pageController.jumpToPage(0);
      _currentPage = 0;
    }
    notifyListeners();
  }
}