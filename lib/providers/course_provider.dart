import 'package:flutter/material.dart';
import 'package:heystars_app/services/json_parser/course/chapters_parser.dart';
import 'package:heystars_app/services/json_parser/course/tutors_parser.dart';

class CourseProvider with ChangeNotifier{

  TutorsParser _tutorsParser;
  ChaptersParser _chaptersParser;
  int _selectedTutor;
  int _selectedChapter;

  TutorsParser get tutorParser => _tutorsParser;
  ChaptersParser get chaptersParser => _chaptersParser;
  int get selectedTutor => _selectedTutor;
  int get selectedChapter => _selectedChapter;

  void setTutorsParser(TutorsParser tutorParser){
    _tutorsParser = tutorParser;
  }

  void setChaptersParser(ChaptersParser chaptersParser){
    _chaptersParser = chaptersParser;
  }

  void setSelectedTutor(int selectedTutor){
    _selectedTutor = selectedTutor;
  }

  void setSelectedChapter(int selectedChapter){
    _selectedChapter = selectedChapter;
  }

}