import 'package:flutter/material.dart';

class UserProvider with ChangeNotifier{
  String _uid;
  String _email;
  String _name;
  String _lang;
  String _birth;
  String _sex;
  String _nickname;
  bool _isLogin;

  String get name => _name;
  String get email => _email;
  String get uid => _uid;
  String get lang => _lang;
  String get birth => _birth;
  String get sex => _sex;
  String get nickname => _nickname;
  bool get isLogin => _isLogin;

  void setUid(String uid){
    _uid = uid;
  }

  void setEmail(String email){
    _email = email;
  }

  void setName(String name){
    _name = name;
  }

  void setLang(String lang){
    _lang = lang;
  }

  void setBirth(String birth){
    _birth = birth;
  }

  void setSex(String sex){
    _sex = sex;
  }

  void setNickname(String nickname){
    _nickname = nickname;
  }

  void setIsLogin(bool isLogin){
    _isLogin = isLogin;
  }

  bool isUserEnterInfo(){
    if(_lang == null || _birth == null || _sex == null || _nickname == null){
      return false;
    }else{
      return true;
    }
  }

  @override
  String toString() {
    return 'uid:$_uid, email:$_email, name:$_name, lang:$_lang, birth:$_birth sex:$_sex, nickname:$_nickname';
  }
}