import 'package:flutter/cupertino.dart';
import 'package:heystars_app/services/json_parser/chatbot/chatbot_json.dart';
import 'package:flutter/material.dart';

class ScenarioListStatus extends ChangeNotifier{
  List<IntroScenario> _introScenariosListStudy;
  List<IntroScenario> _introScenariosListQuiz;
  List<IntroScenario> _introScenariosListFinished;
  List<List<StudyScenarios>> _studyScenarioListStudy;
  List<List<StudyScenarios>> _studyScenarioListRead;
  List<List<CommonScenarios>> _quizScenarioList;
  // 다음 문제로 넘기기 위해 필요한 idx
  int _index;
  List<List<dynamic>> _scenarioList;
  // user가 선택한 답과 발음을 비교하기 위함
  String _userAns;

  List<IntroScenario> get introScenariosListStudy => _introScenariosListStudy;
  List<IntroScenario> get introScenariosListQuiz => _introScenariosListQuiz;
  List<IntroScenario> get introScenariosListFinished => _introScenariosListFinished;
  List<List<StudyScenarios>> get studyScenarioListStudy => _studyScenarioListStudy;
  List<List<StudyScenarios>> get studyScenarioListRead => _studyScenarioListRead;
  List<List<CommonScenarios>> get quizScenarioList => _quizScenarioList;
  int get index => _index;
  List<List<dynamic>> get scenarioList => _scenarioList;
  String get userAns => _userAns;


  void setIntroScenariosListStudy(List<IntroScenario> introScenariosListStudy){
    _introScenariosListStudy = introScenariosListStudy;
    notifyListeners();
  }

  void setIntroScenariosListQuiz(List<IntroScenario> introScenariosListQuiz){
    _introScenariosListQuiz = introScenariosListQuiz;
    notifyListeners();
  }

  void setIntroScenariosListFinished(List<IntroScenario> introScenariosListFinished){
    _introScenariosListFinished = introScenariosListFinished;
    notifyListeners();
  }

  void setStudyScenarioListStudy(List<List<StudyScenarios>> studyScenarioListStudy){
    _studyScenarioListStudy = studyScenarioListStudy;
    notifyListeners();
  }

  void setStudyScenarioListRead(List<List<StudyScenarios>> studyScenarioListRead){
    _studyScenarioListRead = studyScenarioListRead;
    notifyListeners();
  }

  void setQuizScenarioList(List<List<CommonScenarios>> quizScenarioList){
    _quizScenarioList = quizScenarioList;
    notifyListeners();
  }

  // 문제를 다음 문제로 넘기기위해 필요한 index
  void setIndex(int idx){
    _index+=idx;
    notifyListeners();
  }

  // 문제 시작 idx설정
  void setInitIndex(int idx){
    _index = idx;
  }

  // 시나리오 설정
  void setScenarioList(String name){
    if (name =='study'){
      _scenarioList = _studyScenarioListStudy;
    }else{
    _scenarioList = _quizScenarioList;
    }
    notifyListeners();
  }

  void setUserAns(String ans){
    _userAns = ans;
    notifyListeners();
  }

}