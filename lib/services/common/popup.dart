import 'package:flutter/material.dart';

class Popup{

  void privacyPolicyPopup(BuildContext context, Size size, Function goSignUp) {
    showDialog(context: context,
        builder: (BuildContext context){
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)
              ),
              content: Container(
                height: size.height*0.43,
                width: size.width*0.91,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: size.width*0.308,
                      child: Image.asset(
                          'assets/images/signUp/privacy-96px.png'
                      ),
                    ),
                    Text(
                      'Privacy Policy',
                      style: TextStyle(
                          fontSize: 24,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.w800
                      ),
                    ),
                    Text('By using signing up, you confirm that you comply with the privacy policy',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 16,
                          height: 1.6,
                          fontFamily: 'NotoSansCJKkr',
                          fontWeight: FontWeight.w400
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          RaisedButton(
                            child: Text(
                              'CANCEL',
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: 'NotoSansCJKkr',
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xffcccccc)
                              ),
                            ),
                            elevation: 0,
                            color: Colors.white,
                            onPressed: (){
                              Navigator.of(context).pop();
                            },
                          ),
                          RaisedButton(
                            child: Text(
                              'PROCEED',
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff27d794)
                              ),
                            ),
                            elevation: 0,
                            color: Colors.white,
                            onPressed: (){
                              Navigator.of(context).pop();
                              goSignUp();
                              /*Navigator.push(context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) => SignUpPage(auth: widget.auth, onSignedIn: widget.onSignedIn,),
                                )
                            );*/
                            },
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )
          );
        }
    );
  }

  void chatbotStartPopup(BuildContext context, Size size){
    showDialog(
        context: context,
        builder: (BuildContext buildContext){
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20)
            ),
            content: Container(
              height: size.height*0.43,
              width: size.width*0.91,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    'Alert',
                    style: TextStyle(
                        fontSize: 24,
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.w800
                    ),
                  ),
                  Text('Please select both chapter and course',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16,
                        height: 1.6,
                        fontFamily: 'NotoSansCJKkr',
                        fontWeight: FontWeight.w400
                    ),
                  ),
                  RaisedButton(
                    child: Text(
                      'OK',
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w700,
                          color: Color(0xff27d794)
                      ),
                    ),
                    elevation: 0,
                    color: Colors.white,
                    onPressed: (){
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        }
    );
  }
}