
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class DevicePermission{

  /*
  * storage 권한이 있는지 확인 후,
  * 권한 요청
  * */
  Future<void> checkStoragePermission() async{
    PermissionStatus storagePermissionStatus = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.storage);
    if(storagePermissionStatus == PermissionStatus.granted){
      return true;
    }else{
      _reqeustStoragePermission();
      return false;
    }
  }

  /*
  * storage에 대한 권한 요청
  * */
  Future<void> _reqeustStoragePermission() async{
    var result = await PermissionHandler().requestPermissions([PermissionGroup.storage]);
    if(result[PermissionGroup.storage] == PermissionStatus.granted){
      print('Storage permission is granted');
      return true;
    }else{
      print('Storage permission is not granted');
      return false;
    }
  }

  Future<void> checkMicPermission() async{
    PermissionStatus micPermissionStatus = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.microphone);
    if(micPermissionStatus == PermissionStatus.granted){
      return true;
    }else{
      return _requestMicPermission();
    }
  }

  Future<void> _requestMicPermission() async{
    var result = await PermissionHandler().requestPermissions([PermissionGroup.microphone]);
    if(result[PermissionGroup.microphone] == PermissionStatus.granted){
      return true;
    }else{
      return false;
    }
  }
}