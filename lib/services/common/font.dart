import 'package:flutter/cupertino.dart';

const korFont = 'NotoSansCJKkr';
const nkorFont = 'Roboto';

// study 말풍선(한국어부분)
const msgStyle = TextStyle(
    fontFamily: korFont,
    fontWeight: FontWeight.w500,
    fontSize: 12,
    color: Color.fromRGBO(51, 51, 51, 1)
);

// study 말풍선(발음 부분)
const pronStyle = TextStyle(
  fontFamily: nkorFont,
  fontWeight: FontWeight.w400,
  fontSize: 12,
  color: Color.fromRGBO(51, 51, 51, 1),
);
