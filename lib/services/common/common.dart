import 'package:flutter/material.dart';

class Common{
  getCardGradientColor(int i){
    if(i == 0)
      return [Color(0xff86ed50), Color(0xff47adda)];
    else if(i == 1)
      return [Color(0xff00d8ff), Color(0xff7789e2)];
    else if(i == 2)
      return [Color(0xff28608d), Color(0xff9ddbca)];
    else if(i == 3)
      return [Color(0xff30764a), Color(0xffb2ae3a)];
  }
}