import 'package:flutter/material.dart';
import 'package:heystars_app/services/json_parser/chatbot/common_scenario_json.dart';

class ChatbotJson{
  List<IntroScenario> introScenariosListStudy;
  List<IntroScenario> introScenariosListQuiz;
  List<IntroScenario> introScenariosListFinished;
  List<List<StudyScenarios>> studyScenarioListStudy;
  List<List<StudyScenarios>> studyScenarioListRead;
  List<List<CommonScenarios>> quizScenarioList;

  ChatbotJson({ this.studyScenarioListStudy, this.studyScenarioListRead, this.quizScenarioList,
    this.introScenariosListStudy, this.introScenariosListQuiz, this.introScenariosListFinished});

  ChatbotJson.fromJson(Map<String , dynamic> json){
    if(json['introScenariosList'] != null){
      if(json['introScenariosList'][0] != null) {
        introScenariosListStudy = List<IntroScenario>();
        json['introScenariosList'][0].forEach((list) {
          introScenariosListStudy.add(IntroScenario.fromJson(list));
        });
      }
      if(json['introScenariosList'][1] != null){
        introScenariosListQuiz = List<IntroScenario>();
        json['introScenariosList'][1].forEach((list){
          introScenariosListQuiz.add(IntroScenario.fromJson(list));
        });
      }
      if(json['introScenariosList'][2] != null){
        introScenariosListFinished = List<IntroScenario>();
        json['introScenariosList'][2].forEach((list){
          introScenariosListFinished.add(IntroScenario.fromJson(list));
        });
      }
    }

    if (json['studyScenarioList'] != null){
      studyScenarioListStudy = List<List<StudyScenarios>>();
      studyScenarioListRead = List<List<StudyScenarios>>();
      for(int idx=0; idx<json['studyScenarioList'].length; idx++){
        if(idx %2 == 0){
          List<StudyScenarios> temp = [];
          json['studyScenarioList'][idx].forEach((v){
            temp.add(StudyScenarios.fromJson(v));
          });
          studyScenarioListStudy.add(temp);
        }else{
          List<StudyScenarios> temp = [];
          json['studyScenarioList'][idx].forEach((v){
            temp.add(StudyScenarios.fromJson(v));
          });
          studyScenarioListRead.add(temp);
        }
      }
      if(studyScenarioListStudy.length > studyScenarioListRead.length){
        studyScenarioListRead.add(studyScenarioListRead[studyScenarioListRead.length-1]);
      }
    }

    if (json['quizScenarioList'] != null){
      quizScenarioList = List<List<CommonScenarios>>();
      json['quizScenarioList'].forEach((list){
        List<CommonScenarios> temp = [];
        list.forEach((v){
          temp.add(CommonScenarios.fromJson(v));
        });
        quizScenarioList.add(temp);
      });
    }
  }
}

class CommonScenarios{
  String answerKor;
  String answer;
  String responseKor;
  String response;
  String orderIndex;
  String subOrderIndex;
  String mediaUrl;
  String question;

  CommonScenarios({this.answerKor, this.answer, this.responseKor, this.response, this.orderIndex, this.subOrderIndex, this.mediaUrl, this.question});

  CommonScenarios.fromJson(Map<String, dynamic> json){
    answerKor = json['answerKor'];
    answer = json['answer'];
    responseKor = json['responseKor'];
    response = json['response'];
    orderIndex = json['orderIndex'];
    subOrderIndex = json['subOrderIndex'];
    mediaUrl = json['mediaUrl'];
    question = json['question'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic>data = new Map<String, dynamic>();
    data['answerKor'] = this.answerKor;
    data['answer'] = this.answer;
    data['responseKor'] = this.responseKor;
    data['response'] = this.response;
    data['orderIndex'] = this.orderIndex;
    data['subOrderIndex'] = this.subOrderIndex;
    data['mediaUrl'] = this.mediaUrl;
    data['question'] = this.question;
    return data;
  }
}

class StudyScenarios{
  String answerKor;
  String answer;
  String responseKor;
  String response;
  String imgUrl;
  String orderIndex;
  String subOrderIndex;
  String mediaUrl;

  StudyScenarios({this.answerKor, this.answer, this.responseKor, this.response, this.imgUrl, this.orderIndex, this.subOrderIndex, this.mediaUrl});

  StudyScenarios.fromJson(Map<String, dynamic> json){
    answerKor = json['answerKor'];
    answer = json['answer'];
    responseKor = json['responseKor'];
    response = json['response'];
    imgUrl = json['imgUrl'];
    orderIndex = json['orderIndex'];
    subOrderIndex = json['subOrderIndex'];
    mediaUrl = json['mediaUrl'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic>data = new Map<String, dynamic>();
    data['answerKor'] = this.answerKor;
    data['answer'] = this.answer;
    data['responseKor'] = this.responseKor;
    data['response'] = this.response;
    data['orderIndex'] = this.orderIndex;
    data['subOrderIndex'] = this.subOrderIndex;
    data['mediaUrl'] = this.mediaUrl;
    return data;
  }
}

class IntroScenario {
  String answerKor;
  String answer;
  String responseKor;
  String response;
  String orderIndex;
  String subOrderIndex;
  //String mediaUrl;
  List<ComScenarioModel> comModelAnswers;
  ComScenarioModel comModelResponse;

  IntroScenario(
      {this.answerKor,
        this.answer,
        this.responseKor,
        this.response,
        this.orderIndex,
        this.subOrderIndex,
        this.comModelAnswers,
        this.comModelResponse});
  IntroScenario.fromJson(Map<String, dynamic> json) {
    answerKor = json['answerKor'];
    answer = json['answer'];
    responseKor = json['responseKor'];
    response = json['response'];
    orderIndex = json['orderIndex'];
    subOrderIndex = json['subOrderIndex'];
    if (json['comModelAnswers'] != null) {
      comModelAnswers = new List<ComScenarioModel>();
      json['comModelAnswers'].forEach((v) {
        comModelAnswers.add(new ComScenarioModel.fromJson(v));
      });
    }
    if (json['comModelResponse'] != null) {
      comModelResponse = new ComScenarioModel.fromJson(json['comModelResponse']);
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['answerKor'] = this.answerKor;
    data['answer'] = this.answer;
    data['responseKor'] = this.responseKor;
    data['response'] = this.response;
    data['orderIndex'] = this.orderIndex;
    data['subOrderIndex'] = this.subOrderIndex;
    if (this.comModelAnswers != null) {
      data['comModelAnswers'] = this.comModelAnswers.map((v) => v.toJson()).toList();
    }
    if(this.comModelResponse != null){
      data['comModelResponse'] = this.comModelResponse.toJson();
    }
    return data;
  }
}
