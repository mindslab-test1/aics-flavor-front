class SttRes{
  String status;
  String data;

  SttRes({this.status, this.data});

  SttRes.fromJson(Map<String, dynamic> json){
    status = json['status'];
    data = json['data'];
  }


  Map<String, dynamic> toJson(){
    final Map<String, dynamic>response = new Map<String, dynamic>();
    response['answerKor'] = this.status;
    response['answer'] = this.data;
    return response;
  }

}