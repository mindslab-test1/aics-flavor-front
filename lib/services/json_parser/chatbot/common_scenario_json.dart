import 'package:heystars_app/services/json_parser/chatbot/chatbot_json.dart';
import 'package:heystars_app/services/json_parser/chatbot/media_json.dart';

class ComScenarioModel {
  int id;
  String division;
  String korText;
  String engText;
  String vietTExt;
  String indoText;
  int orderIndex;
  String imgUrl;
  Media mediaModel;
  ComScenarioModel(
      {this.id,
        this.division,
        this.korText,
        this.engText,
        this.vietTExt,
        this.indoText,
        this.orderIndex,
        this.imgUrl,
        this.mediaModel});
  ComScenarioModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    division = json['division'];
    korText = json['korText'];
    engText = json['engText'];
    vietTExt = json['vietTExt'];
    indoText = json['indoText'];
    orderIndex = json['orderIndex'];
    imgUrl = json['imgUrl'];
    mediaModel = json['mediaModel'] != null ? new Media.fromJson(json['mediaModel']) : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['division'] = this.division;
    data['korText'] = this.korText;
    data['engText'] = this.engText;
    data['vietTExt'] = this.vietTExt;
    data['indoText'] = this.indoText;
    data['orderIndex'] = this.orderIndex;
    data['imgUrl'] = this.imgUrl;
    if (this.mediaModel != null) {
      data['mediaModel'] = this.mediaModel.toJson();
    }
    return data;
  }
}