class Media {
  int id;
  String name;
  String type;
  String url;
  int tutorID;
  Media({this.id, this.name, this.type, this.url, this.tutorID});
  Media.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    type = json['type'];
    url = json['url'];
    tutorID = json['tutorID'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['type'] = this.type;
    data['url'] = this.url;
    data['tutorID'] = this.tutorID;
    return data;

  }
}