class ChaptersParser{

  List<Chapter> chapters;

  ChaptersParser({this.chapters});

  ChaptersParser.fromJson(Map<String, dynamic> json) {
    if (json['chapters'] != null) {
      chapters = new List<Chapter>();
      json['chapters'].forEach((v) {
        chapters.add(new Chapter.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.chapters != null) {
      data['chapters'] = this.chapters.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Chapter{
  int id;
  String titleKor;
  String titleEng;
  int levelNum;
  String imgUrl;

  Chapter({
    this.id,
    this.titleKor,
    this.titleEng,
    this.levelNum,
    this.imgUrl
  });

  Chapter.fromJson(Map<String, dynamic> json){
    id = json['id'];
    titleKor = json['titleKor'];
    titleEng = json['titleEng'];
    levelNum = json['levelNum'];
    imgUrl = json['imgUrl'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['titleKor'] = this.titleKor;
    data['titleEng'] = this.titleEng;
    data['levelNum'] = this.levelNum;
    data['imgUrl'] = this.imgUrl;
    return data;
  }
}