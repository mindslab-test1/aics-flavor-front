import 'package:flutter/material.dart';

class TutorsParser{
  List<Tutor> tutors;

  TutorsParser({this.tutors});

  TutorsParser.fromJson(Map<String, dynamic> json){
    if(json['tutors'] != null){
      tutors = List<Tutor>();
      json['tutors'].forEach((tutor){
        tutors.add(Tutor.fromJson(tutor));
      });
    }
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if(this.tutors != null){
      data['tutor'] = this.tutors.map((v) => v.toJson()).toList();
    }
  }
}

class Tutor{
  int id;
  String name;
  String description;
  String job;
  String member;
  String imgUrl;

  Tutor({
    this.id,
    this.name,
    this.description,
    this.job,
    this.member,
    this.imgUrl});

  Tutor.fromJson(Map<String, dynamic> json){
    id = json['id'];
    name = json['name'];
    description = json['description'];
    job = json['job'];
    member = json['member'];
    imgUrl = json['imgUrl'];
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['job'] = this.job;
    data['member'] = this.member;
    data['imgUrl'] = this.imgUrl;
    return data;
  }
}