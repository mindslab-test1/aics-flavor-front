
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

abstract class AuthFunc{
  Future<String> signIn(String email, String password);

  Future<String> signUp(String email, String password);

  Future<User> getCurrentUser();

  Future<void> sendEmailVerification();

  Future<void> signOut();

  Future<bool> isEmailVerified();

  Future<String> googleSignInFunc();
}

class MyAuth implements AuthFunc{
  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final _googleSignIn = GoogleSignIn();

  @override
  Future<User> getCurrentUser() async{
    return await _firebaseAuth.currentUser;
  }

  @override
  Future<void> sendEmailVerification() async{
    var user = await _firebaseAuth.currentUser;
    user.sendEmailVerification();
  }

  @override
  Future<String> signIn(String email, String password) async{
    var user = (await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password)).user;
    return user.uid;
  }

  @override
  Future<void> signOut() {
    return _firebaseAuth.signOut();
  }

  @override
  Future<String> signUp(String email, String password) async{
    var user = (await _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password)).user;
    return user.uid;
  }

  @override
  Future<bool> isEmailVerified() async{
    var user = await _firebaseAuth.currentUser;
    return user.emailVerified;
  }

  @override
  Future<String> googleSignInFunc() async{
    GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();

    if(googleSignInAccount != null){
      GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;

      AuthCredential credential = GoogleAuthProvider.credential(
        idToken: googleSignInAuthentication.idToken,
        accessToken: googleSignInAuthentication.accessToken);

      await _firebaseAuth.signInWithCredential(credential);

      var user = await _firebaseAuth.currentUser;

      return Future.value(user.uid);
    }
  }
  
}