import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class SttRepository{

  String _apiServer = DotEnv().env['API_SERVER'];
  String _PATH = '/stt';

  Future callSttApi(File file) async{

    FormData formData = FormData.fromMap({
      "file" : await MultipartFile.fromFile(file.path)
    });
    return await Dio().post(_apiServer+_PATH+'/maumStt', data: formData);
  }
}