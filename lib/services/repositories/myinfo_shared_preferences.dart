import 'package:shared_preferences/shared_preferences.dart';


class SharedPrefs{
  static SharedPreferences _sharedPreferences;

  init()async{
    if(_sharedPreferences ==null){
      _sharedPreferences = await SharedPreferences.getInstance();
    }
    if(alarmCheck == null){
      _sharedPreferences.setBool('alarmCheck', true);
    }
    if (alarmDate == null) {
      _sharedPreferences.setString('alarmDate', 'Time');
    }
    if(languageIndex == null){
      _sharedPreferences.setInt('languageIndex', 0);
    }
    if(alarmDateOrigin ==null){
      _sharedPreferences.setString('alarmDateOrigin', DateTime.now().toIso8601String());
    }
    if(alarmDaysCheck ==null){
      _sharedPreferences.setStringList('alarmDaysCheck', ["0","0","0","0","0","0","0", ]);
    }
  }

  static final SharedPrefs _sharedPrefs = new SharedPrefs._internal();

  factory SharedPrefs(){
    return _sharedPrefs;
  }

  SharedPrefs._internal(){
     _sharedPreferences;
  }

  //alarm
  bool get alarmCheck => _sharedPreferences.getBool('alarmCheck');
  List<String> get alarmDays => _sharedPreferences.getStringList('alarmDays');
  String get alarmDate => _sharedPreferences.getString('alarmDate');
  List<String> get alarmDaysCheck => _sharedPreferences.getStringList('alarmDaysCheck');
  String get alarmDateOrigin => _sharedPreferences.getString('alarmDateOrigin');

  //lang
  String get languageImg => _sharedPreferences.getString('languageImg');
  String get languageName =>_sharedPreferences.getString('languageName');
  String get languageNation => _sharedPreferences.getString('languageNation');
  int get languageIndex => _sharedPreferences.getInt('languageIndex');

  //voice
  String get voiceName => _sharedPreferences.getString('voiceName');
  String get voiceImg =>_sharedPreferences.getString('voiceImg');
  int get voiceIndex => _sharedPreferences.getInt('voiceIndex');

  //alarm
  set setAlarmCheck(bool value){
    _sharedPreferences.setBool('alarmCheck', value);
  }

  set setAlarmDate(String value){
    _sharedPreferences.setString('alarmDate', value);
  }

  set setAlarmDateOrigin(String time){
    _sharedPreferences.setString('alarmDateOrigin', time);
  }

  set setAlarmDaysCheck(List<String> value){
   _sharedPreferences.setStringList('alarmDaysCheck', value);
  }

  set setAlarmDays(String value){
    if(_sharedPreferences.getStringList('alarmDays').contains(value) == false){
      _sharedPreferences.getStringList('alarmKeys').add(value);
    }
  }

  set popAlarmDays(String value){
    _sharedPreferences.getStringList('alarmDays').remove(value);
  }

  //voice
  set setVoiceName(String value){
    _sharedPreferences.setString('voiceName', value);
  }

  set setVoiceImg(String value){
    _sharedPreferences.setString('voiceImg', value);
  }

  set setVoiceIndex(int value){
    _sharedPreferences.setInt('voiceIndex', value);
  }

  //lang
  set setLanguageName(String value){
    _sharedPreferences.setString('languageName', value);
  }

  set setLanguageImg(String value){
    _sharedPreferences.setString('languageImg', value);
  }

  set setLanguageIndex(int value){
    _sharedPreferences.setInt('languageIndex', value);
  }

  set setLanguageNation(String value){
    if (value == 'English'){
      _sharedPreferences.setString('languageNation', 'U.S.A');
    }else if(value =='Indonesian'){
      _sharedPreferences.setString('languageNation', 'Indonesia');
    }else{
      _sharedPreferences.setString('languageNation', 'Vietnam');
    }
  }



}
