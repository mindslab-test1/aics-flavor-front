import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class CourseRepository{

  String _apiServer = DotEnv().env['API_SERVER'];
  String PATH = '/course';

  Future<Map> getTutorsAndChapters() async{

    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
    };

    final response = await http.get(
      _apiServer+PATH+'/getTutorsAndChapters',
      headers: headers);

    return jsonDecode(response.body);
  }

  Future getScenarioAll(String scenario, String userLang) async{
    return await http.get(_apiServer+PATH+'/getScenario/'+scenario + '/'+ userLang);
  }
}