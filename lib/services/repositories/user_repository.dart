import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class UserRepository{
  String _apiServer = DotEnv().env['API_SERVER'];
  String PATH = '/account';

  /*
  * 유저의 개인정보 저장
  * */
  Future<int> saveUserInfoPost(String uid, String email, String nickname, String birth, String sex, String lang) async{
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": uid
    };


    Map<String, String> param ={
      'email' : email,
      'nickname' : nickname,
      'birth' : birth,
      'sex' : sex,
      'lang' : lang
    };

    final response = await http.post(
        _apiServer+PATH+'/saveUserInfo',
        headers: headers,
        body: jsonEncode(param));

    return response.statusCode;
  }

  /*
  * 유저 정보 조회
  * */
  Future getUserInfoPost(String uid) async{

    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": uid
    };

    return http.get(_apiServer+PATH+'/getUserInfo',
        headers: headers);
  }

  /*
  * 유저 정보 수정
  * */
  Future putUserLang(String uid, String lang) async{
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": uid
    };

    Map<String, String> param = {
      'lang' :lang
    };


    final response = await http.post(
        _apiServer+PATH+'/putUserLang',
        headers: headers,
        body: jsonEncode(param));

    return response.statusCode;

  }


}
