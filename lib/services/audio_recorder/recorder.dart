import 'dart:io' as io;
import 'package:flutter/material.dart';
import 'package:heystars_app/services/audio_recorder/flutter_audio_recorder.dart';
import 'package:path_provider/path_provider.dart';
import 'package:file/file.dart';
import 'package:file/local.dart';


class Recorder{
  final LocalFileSystem localFileSystem;
  Recorder({
    this.localFileSystem
  });

  FlutterAudioRecorder _recorder;
  Recording current;
  RecordingStatus currentStatus = RecordingStatus.Unset;
  File file;

  dispose() async{
    _recorder.stop();
    /*if(file != null){
      await file.delete();
    }*/
  }

  init() async{

    if(file != null){
      await file.delete();
    }

    try{
      if(await FlutterAudioRecorder.hasPermissions){
        String customPath = '/flutter_audio_recorder_';
        io.Directory appDocDirectory;

        if(io.Platform.isIOS){
          appDocDirectory = await getApplicationDocumentsDirectory();
        }else{
          appDocDirectory = await getExternalStorageDirectory();
        }

        customPath = appDocDirectory.path + customPath + DateTime.now().millisecondsSinceEpoch.toString();

        _recorder = FlutterAudioRecorder(customPath, audioFormat: AudioFormat.WAV);

        await _recorder.initialized;

        var currentRecord = await _recorder.current(channel: 0);

        current = currentRecord;
        currentStatus = currentRecord.status;


      }else{
        return 'you dont have permissions';
      }
    }catch (e){
      print(e);
    }
  }

  start() async{
    try{
      await _recorder.start();
      var recording = await _recorder.current(channel: 0);
      current = recording;
      currentStatus = current.status;

    }catch(e){
      print(e);
    }
  }

  resume() async{
    await _recorder.resume();
  }

  pause() async{
    await _recorder.pause();
  }

  stop() async{
    var result = await _recorder.stop();
    File resultFile = localFileSystem.file(result.path);
    current = result;
    currentStatus = current.status;
    file = resultFile;
  }

  changeRecordStatus() async{

    switch(currentStatus){
      case RecordingStatus.Initialized:
        {
          await start();
          break;
        }
      case RecordingStatus.Recording:
        {
          await stop();
          break;
        }
      case RecordingStatus.Stopped:
        {
          await init();
          break;
        }
    }
  }
}