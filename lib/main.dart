import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:heystars_app/providers/course_provider.dart';
import 'package:heystars_app/providers/main_paging_provider.dart';
import 'package:heystars_app/providers/myinfo_provider.dart';
import 'package:heystars_app/providers/scenario_list_status.dart';
import 'package:heystars_app/providers/user_provider.dart';
import 'package:heystars_app/screens/layouts/intro_page/intro_main.dart';
import 'package:heystars_app/screens/layouts/main_page.dart';
import 'package:heystars_app/services/login_auth/firebase_auth_utils.dart';
import 'package:provider/provider.dart';

void main() async{
  await DotEnv().load('.env');

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => UserProvider()),
        ChangeNotifierProvider(create: (context) => CourseProvider(),),
        ChangeNotifierProvider(create: (context) => MainPagingProvider(),),
        ChangeNotifierProvider(create: (context)=> ScenarioListStatus(),),
        ChangeNotifierProvider(create: (context) => MyInfoProvider(),)
      ],
      child: MaterialApp(
        routes: {
          '/main': (_) => MainPage(),
        },

        home: FutureBuilder(
          future: Firebase.initializeApp(),
          builder: (context, snapshot){
            if(snapshot.hasError){
              return Container(
                child: Text("firebase error"),
              );
            }
            if(snapshot.connectionState == ConnectionState.done){
              return new IntroMain(auth: MyAuth(),);
            }

            return Container(child: Text('loading...'),);
          },
        ),
      ),
    );
  }
}



