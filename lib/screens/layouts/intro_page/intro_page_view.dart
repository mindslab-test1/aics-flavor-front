import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:heystars_app/providers/user_provider.dart';
import 'package:heystars_app/screens/layouts/intro_page/intro_main.dart';
import 'package:heystars_app/screens/layouts/intro_page/personal_info_main.dart';
import 'package:heystars_app/screens/layouts/login_page/signIn_page.dart';
import 'package:heystars_app/screens/layouts/login_page/signup_page.dart';
import 'package:heystars_app/services/login_auth/firebase_auth_utils.dart';
import 'package:provider/provider.dart';

class IntroPage extends StatefulWidget {
  AuthFunc auth;
  Function goMainPage;
  VoidCallback onSignedIn;
  IntroPage({this.auth, this.onSignedIn, this.goMainPage});

  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> with SingleTickerProviderStateMixin{
  UserProvider userProvider;
  bool selected = true;
  bool isSignUp = false;

  Timer _timer;
  int _start = 1;

  void startTimer(){
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
          (Timer timer) => setState(
            () {
          if (_start < 1) {
            selected = false;
            timer.cancel();
          } else {
            _start = _start - 1;
          }
        },
      ),
    );
  }

  @override
  void initState() {
    userProvider = Provider.of(context, listen: false);

    super.initState();
    startTimer();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: SizedBox.expand(
        child: Stack(
          children: [
            SizedBox.expand(
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      Color(0xff86ed50),
                      Color(0xff47adda)
                    ]
                  )
                ),

                child: Padding(
                  padding: EdgeInsets.only(left: size.width*0.067),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: size.height*0.167,
                      ),
                      Text(
                        'Flavor',
                        style: TextStyle(
                          fontSize: 32,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.w800,
                          color: Colors.white
                        ),
                      ),
                      SizedBox(
                        height: size.height*0.424,
                      ),
                      SvgPicture.asset(
                        'assets/images/intro/ico_sound.svg'
                      ),
                      SizedBox(
                        height: size.height*0.037,
                      ),
                      Text(
                        'Pick Your Tutor',
                        style: TextStyle(
                          fontSize: 32,
                          color: Colors.white,
                          fontFamily: 'SpoqaHanSans',
                          fontWeight: FontWeight.w700
                        ),
                      ),
                      Text(
                        'Your Personal Korean Tutor',
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.white,
                          fontFamily: 'SpoqaHanSans',
                          fontWeight: FontWeight.w400
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                child: Container(
                  height: size.height*0.728,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: AnimatedSize(
                      duration: const Duration(milliseconds: 500),
                      curve: Curves.fastOutSlowIn,
                      child: Container(
                        height: selected ? 0 : size.height*0.728,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(16),
                            topLeft: Radius.circular(16)
                          )
                        ),
                        child: SingleChildScrollView(
                          child: userProvider.isLogin
                              ?GetPersonalInfoMain(
                                goMainPage: widget.goMainPage,
                              )
                              :(isSignUp==true)
                                    ?SignUpPage(
                                      auth: widget.auth,
                                      onSignedIn: widget.onSignedIn,
                                      goSignIn: (){
                                        setState(() {
                                          isSignUp = false;
                                        });
                                      },)
                                    :SignInPage(
                                      auth: widget.auth,
                                      onSignedIn: widget.onSignedIn,
                                      goSignUp: (){
                                        setState(() {
                                          isSignUp = true;
                                        }
                                      );},)
                        ),
                      ),
                      vsync: this,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
