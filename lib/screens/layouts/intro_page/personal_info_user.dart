import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:heystars_app/providers/user_provider.dart';
import 'package:heystars_app/services/repositories/user_repository.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

enum Sex{
  Male,
  Female
}

class PersonalInfoUser extends StatefulWidget {
  Function goMainPage;
  PersonalInfoUser({
    this.goMainPage
  });

  @override
  _PersonalInfoUserState createState() => _PersonalInfoUserState();
}

class _PersonalInfoUserState extends State<PersonalInfoUser> {

  final _formKey = GlobalKey<FormState>();

  String _nickname, _birth;

  Sex selectedSex;
  UserProvider userProvider;

  @override
  void initState() {
    super.initState();
    userProvider = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(24, 36, 24, 0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Who are you?',
                style: TextStyle(
                  fontSize: 24,
                  color: Color(0xff333333),
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w800
                ),
              ),
              SizedBox(height: 4,),
              Text(
                'Tell me your nickname',
                style: TextStyle(
                  color: Color(0xff999999),
                  fontFamily: 'NotoSansCJKkr',
                  fontWeight: FontWeight.w400,
                  fontSize: 12
                ),
              ),
              SizedBox(height: 24,),
              Text(
                'Nickname',
                style: TextStyle(
                    color: Color(0xff999999),
                    fontFamily: 'NotoSansCJKkr',
                    fontWeight: FontWeight.w500,
                    fontSize: 12
                ),
              ),
              SizedBox(height: 4,),
              Container(
                decoration: BoxDecoration(
                  color: Color(0xfffcfcfc),
                  borderRadius: BorderRadius.circular(8)
                ),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  maxLines: 1,
                  autofocus: false,
                  decoration: InputDecoration(
                    fillColor: Color(0xffcccccc),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xfff5f5f5)),
                      borderRadius: BorderRadius.circular(8)
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xfff5f5f5)),
                      borderRadius: BorderRadius.circular(8)
                    ),
                    hintText: 'Nickname(English only)',
                    hintStyle: TextStyle(
                      fontSize: 14,
                      color: Color(0xffcccccc)
                    )
                  ),
                  validator: (value){
                    return value.isEmpty?'nickname can not be empty': null;
                  },
                  onSaved: (value){
                    _nickname = value;
                  },
                ),
              ),

              SizedBox(height: 16,),

              Text(
                'Birth',
                style: TextStyle(
                    color: Color(0xff999999),
                    fontFamily: 'NotoSansCJKkr',
                    fontWeight: FontWeight.w500,
                    fontSize: 12
                ),
              ),
              SizedBox(height: 4,),
              Container(
                decoration: BoxDecoration(
                  color: Color(0xfffcfcfc),
                  borderRadius: BorderRadius.circular(8)
                ),
               child: TextFormField(
                 keyboardType: TextInputType.datetime,
                 maxLines: 1,
                 autofocus: false,
                 decoration: InputDecoration(
                   fillColor: Color(0xffcccccc),
                   focusedBorder: OutlineInputBorder(
                     borderSide: BorderSide(color: Color(0xfff5f5f5)),
                     borderRadius: BorderRadius.circular(8)
                   ),
                   enabledBorder: OutlineInputBorder(
                     borderSide: BorderSide(color: Color(0xfff5f5f5)),
                     borderRadius: BorderRadius.circular(8)
                   ),
                   hintText: 'DD / MM / YYYY',
                   hintStyle: TextStyle(
                     fontSize: 14,
                     color: Color(0xffcccccc),
                     fontFamily: 'NotoSansCJKkr',
                     fontWeight: FontWeight.w400
                   ),
                 ),
                 validator: (value) {
                   return value.isEmpty? 'birth can not be empty' : null;
                 },
                 onSaved: (value){
                   _birth = value;
                 },
               ),
              ),

              SizedBox(height: 16,),

              Text(
                'Sex',
                style: TextStyle(
                    color: Color(0xff999999),
                    fontFamily: 'NotoSansCJKkr',
                    fontWeight: FontWeight.w500,
                    fontSize: 12
                ),
              ),
              SizedBox(height: 4,),
              Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Flexible(
                    flex: 1,
                    child: GestureDetector(
                      onTap: (){
                        setState(() {
                          selectedSex = Sex.Male;
                        });
                      },
                      child: Container(
                        height: 55,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(8),
                            bottomLeft: Radius.circular(8)
                          ),
                          border: Border.all(
                            color: (selectedSex == Sex.Male)
                                ?Color(0xff27d794)
                                :Color(0xffdddddd)
                          )
                        ),
                        child: SizedBox.expand(
                          child: Center(
                            child: Text(
                              Sex.Male.toString().split('.')[1],
                              style: TextStyle(
                                color: (selectedSex == Sex.Male)
                                    ?Color(0xff27d794)
                                    :Color(0xffdddddd),
                                fontFamily: 'NotoSansCJKkr',
                                fontWeight: FontWeight.w400
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: GestureDetector(
                      onTap: (){
                        setState(() {
                          selectedSex = Sex.Female;
                        });
                      },
                      child: Container(
                        height: 55,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: (selectedSex == Sex.Female)
                                ?Color(0xff27d794)
                                :Color(0xffdddddd)
                          ),
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(8),
                              bottomRight: Radius.circular(8)
                          ),
                        ),
                        child: SizedBox.expand(
                          child: Center(
                            child: Text(
                              Sex.Female.toString().split('.')[1],
                              style: TextStyle(
                                  color: (selectedSex == Sex.Female)
                                      ?Color(0xff27d794)
                                      :Color(0xffdddddd),
                                  fontFamily: 'NotoSansCJKkr',
                                  fontWeight: FontWeight.w400
                              )
                            ),
                          ),
                        )
                      ),
                    ),
                  )
                ],
              ),

              SizedBox(height: 24,),

              Container(
                width: double.infinity,
                child: RaisedButton(
                  color: Color(0xff27d794),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)
                  ),
                  elevation: 0,
                  onPressed: saveAndFinish,
                  child: Padding(
                      padding: EdgeInsets.fromLTRB(12, 16, 12, 16),
                      child: Text(
                        'Save',
                        style: TextStyle(
                            fontSize: 16,
                            color: Colors.white
                        ),
                      )
                  ),
                ),
              ),

              SizedBox(height: 24,),

              Container(
                width: double.infinity,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'By Typing in, you agreed to ',
                      style: TextStyle(
                        fontFamily: 'NotoSansCJKkr',
                        fontWeight: FontWeight.w400,
                        fontSize: 13
                      ),
                    ),
                    GestureDetector(
                      child: Text(
                        'Privacy Policy',
                        style: TextStyle(
                            fontFamily: 'NotoSansCJKkr',
                            fontWeight: FontWeight.w500,
                            fontSize: 13,
                            color: Color(0xff27d794),
                            decoration: TextDecoration.underline
                        )
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  saveAndFinish() async{
    final form = _formKey.currentState;

    if(form.validate() && selectedSex != null){
      form.save();

      setState(() {
        userProvider.setNickname(_nickname);
        userProvider.setBirth(_birth);

        if(selectedSex == Sex.Male){
          userProvider.setSex('M');
        }else if(selectedSex == Sex.Female){
          userProvider.setSex('F');
        }
      });

      int statusCode = await UserRepository().saveUserInfoPost(userProvider.uid, userProvider.email,
          userProvider.nickname, userProvider.birth, userProvider.sex, userProvider.lang);

      if(statusCode == 200){
        widget.goMainPage();
      }
    }
  }
}
