import 'package:flutter/material.dart';
import 'package:heystars_app/providers/user_provider.dart';
import 'package:provider/provider.dart';

enum Language {
  English,
  Indonesian,
  Vietnamese
}

class PersonalInfoLanguage extends StatefulWidget {
  final Function goNextPage;

  PersonalInfoLanguage({
    this.goNextPage
  });

  @override
  _PersonalInfoLanguageState createState() => _PersonalInfoLanguageState();
}

class _PersonalInfoLanguageState extends State<PersonalInfoLanguage> {

  Language selectedLang;
  UserProvider userProvider;

  @override
  void initState() {
    super.initState();
    userProvider = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(24, 36, 24, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Choose your language",
              style: TextStyle(
                  fontSize: 24,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w800,
                  color: Color(0xff333333)
              ),
            ),
            SizedBox(
              height: 4,
            ),
            Text(
              "What language do you prefer?",
              style: TextStyle(
                  fontSize: 12,
                  fontFamily: 'NotoSansCJKkr',
                  fontWeight: FontWeight.w400,
                  color: Color(0xff999999)
              ),
            ),
            SizedBox(height: 24,),
            languageRow(size),
            SizedBox(height: 24,),
            nextBtn(size)
          ],
        ),
      ),
    );
  }

  Widget languageRow(Size size){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        languageCard(
            size,
            Language.English,
            'assets/images/intro/america-selected.png',
            'assets/images/intro/america-unselected.png'),
        languageCard(
            size,
            Language.Indonesian,
            'assets/images/intro/indonesia_selected.png',
            'assets/images/intro/indonesia-unselected.png'),
        languageCard(
            size,
            Language.Vietnamese,
            'assets/images/intro/vietnam-selected.png',
            'assets/images/intro/vietnam-unselected.png')
      ],
    );
  }

  languageCard(Size size, Language lang, String selectedImgUrl, String unSelectedImgUrl){
    return GestureDetector(
      onTap: (){
        setState(() {
          selectedLang = lang;
        });
      },
      child: Container(
        width: size.width*0.267,
        height: size.height*0.245,
        decoration: BoxDecoration(
          border: Border.all(
              color: (selectedLang == lang)
                  ?Color(0xff27d794)
                  :Color(0xffdddddd)
          ),
          borderRadius: BorderRadius.all(
              Radius.circular(8)
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(
                (selectedLang == lang)
                    ?selectedImgUrl
                    :unSelectedImgUrl
            ),
            Text(
              lang.toString().split('.').last,
              style: TextStyle(
                  fontSize: 14,
                  color: (selectedLang == lang)
                      ?Color(0xff27d794)
                      :Color(0xffdddddd)
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget nextBtn(Size size){
    return Container(
      width: double.infinity,
      child: RaisedButton(
        color: Color(0xff27d794),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        elevation: 0,
        onPressed: saveAndGoNextPage,
        child: Padding(
            padding: EdgeInsets.fromLTRB(12, 16, 12, 16),
            child: Text(
              'Next',
              style: TextStyle(
                  fontSize: 16,
                  color: Colors.white
              ),
            )
        ),
      ),
    );
  }

  void saveAndGoNextPage(){
    setState(() {
      if(selectedLang == Language.English)
        userProvider.setLang("Eng");
      else if(selectedLang == Language.Indonesian)
        userProvider.setLang("Indo");
      else if(selectedLang == Language.Vietnamese)
        userProvider.setLang("Viet");
    });

    widget.goNextPage();
  }
}
