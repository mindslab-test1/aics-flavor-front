import 'package:flutter/material.dart';
import 'package:heystars_app/providers/user_provider.dart';
import 'package:heystars_app/screens/layouts/intro_page/personal_info_language.dart';
import 'package:heystars_app/screens/layouts/intro_page/personal_info_user.dart';
import 'package:provider/provider.dart';

class GetPersonalInfoMain extends StatefulWidget {
  Function goMainPage;
  GetPersonalInfoMain({
    this.goMainPage
  });

  @override
  _GetPersonalInfoMainState createState() => _GetPersonalInfoMainState();
}

class _GetPersonalInfoMainState extends State<GetPersonalInfoMain> {

  int page = 0;

  @override
  void initState() {
    super.initState();
  }

  goNextPage(){
    setState(() {
      page = page+1;
    });
  }

  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;

    return Container(
      child: (page==0)
          ?PersonalInfoLanguage(goNextPage: goNextPage,)
          :PersonalInfoUser(goMainPage: widget.goMainPage,),
    );
  }
}
