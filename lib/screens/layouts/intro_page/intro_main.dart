import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:heystars_app/providers/user_provider.dart';
import 'package:heystars_app/screens/layouts/intro_page/intro_page_view.dart';
import 'package:heystars_app/screens/layouts/login_page/signIn_page.dart';
import 'package:heystars_app/services/common/device_permission.dart';
import 'package:heystars_app/services/login_auth/firebase_auth_utils.dart';
import 'package:heystars_app/services/repositories/user_repository.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

import '../main_page.dart';

class IntroMain extends StatefulWidget {
  IntroMain({this.auth});

  AuthFunc auth;

  @override
  _IntroMainState createState() => _IntroMainState();
}

enum AuthStatus{
  NOT_LOGIN,
  NOT_DETERMINED,
  LOGIN_HAVE_INFO,
  LOGIN_HAVE_NOT_INFO
}

class _IntroMainState extends State<IntroMain> {

  AuthStatus authStatus = AuthStatus.NOT_DETERMINED;
  String _userId = "", _userEmail="";
  UserProvider userProvider;

  /*
  * firebase에서 유정정보 조회
  * */
  Future<void> getCurrentUser() async{

    await widget.auth.getCurrentUser().then((user)
    {
      setState(() {
        if(user != null){
          userProvider.setUid(user?.uid);
          userProvider.setEmail(user?.email);
          userProvider.setName(user?.displayName);
          userProvider.setIsLogin(true);

          getUserInfo();
        }else{
          userProvider.setIsLogin(false);
          authStatus = AuthStatus.NOT_LOGIN;
        }
      });
    });
  }

  /*
  * 유저가 로그인된 유저인지 확인 후,
  * db에 해당 유저의 데이터가 있는지 확인
  * 데이터가 있으면 provider에 넣어 줌
  * */
  Future<void> getUserInfo() async{
    setState(() {
      authStatus = AuthStatus.NOT_DETERMINED;
    });

    final response = await UserRepository().getUserInfoPost(userProvider.uid);


    setState(() {
      if(userProvider.isLogin){
        if(response.body.isEmpty){
          authStatus = AuthStatus.LOGIN_HAVE_NOT_INFO;
        }else {
          authStatus = AuthStatus.LOGIN_HAVE_INFO;
          final userInfo = jsonDecode(response.body);

          userProvider.setNickname(userInfo['nickname']);
          userProvider.setBirth(userInfo['birth']);
          userProvider.setSex(userInfo['sex']);
          userProvider.setLang(userInfo['lang']);
        }
      }else{
        authStatus = AuthStatus.NOT_LOGIN;
      }
    });
  }

  @override
  void initState() {
    super.initState();
    userProvider = Provider.of(context, listen: false);

    getCurrentUser();
    DevicePermission().checkStoragePermission();

  }

  @override
  Widget build(BuildContext context) {
    switch(authStatus)
    {
      case AuthStatus.NOT_DETERMINED:
        return _showLoading();
        break;
      case AuthStatus.LOGIN_HAVE_NOT_INFO:
      case AuthStatus.NOT_LOGIN:
        return IntroPage(
          auth: widget.auth,
          onSignedIn: _onSignedIn,
          goMainPage: goMainPage,
        );
        break;
      case AuthStatus.LOGIN_HAVE_INFO:
        if(_userId != null)
          return MainPage(
              auth: widget.auth,
              onSignOut: _onSignOut);
        else
          return _showLoading();
        break;
      default:
        return _showLoading();
        break;
    }
  }

  /*
  * 수동으로 사용자가 직접 로그인할 경우
  * */
  void _onSignedIn(){
    widget.auth.getCurrentUser().then((user){
      setState(() {
        _userId = user.uid.toString();
        _userEmail = user.email.toString();
        userProvider.setIsLogin(true);
        userProvider.setEmail(_userEmail);
        userProvider.setUid(_userId);
      });

      setState(() {
        getUserInfo();
      });
    });
  }

  void _onSignOut(){
    setState(() {
      authStatus = AuthStatus.NOT_LOGIN;
      _userId = _userEmail = "";
      userProvider.setIsLogin(false);
    });
  }

  void goMainPage(){
    setState(() {
      if(userProvider.isUserEnterInfo()) {
        print('userProvider isUserEnterInfo ${userProvider.isUserEnterInfo()}');
        authStatus = AuthStatus.LOGIN_HAVE_INFO;
      }
    });
  }
}

Widget _showLoading() {
  return Scaffold(
    body: Container(
      alignment: Alignment.center,
      child: CircularProgressIndicator(),
    ),
  );
}