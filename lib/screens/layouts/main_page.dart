import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:heystars_app/providers/course_provider.dart';
import 'package:heystars_app/providers/main_paging_provider.dart';
import 'package:heystars_app/providers/user_provider.dart';
import 'package:heystars_app/screens/layouts/course_page/course_main.dart';
import 'package:heystars_app/screens/layouts/myinfo_page/myinfo_main.dart';
import 'package:heystars_app/screens/layouts/tutor_page/tutor_main.dart';
import 'package:heystars_app/services/login_auth/firebase_auth_utils.dart';
import 'package:provider/provider.dart';

import 'home_page/home_main.dart';

class MainPage extends StatefulWidget {

  AuthFunc auth;
  VoidCallback onSignOut;

  MainPage({
    Key key,
    this.auth,
    this.onSignOut,
  }):super(key:key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  int currentTab = 0;
  bool _isEmailVerified = false;
  UserProvider userProvider;
  MainPagingProvider mainPagingProvider;
  CourseProvider courseProvider;
  PageController pageController = PageController();



  @override
  void initState() {
    super.initState();
    userProvider = Provider.of<UserProvider>(context, listen: false);
    courseProvider = Provider.of<CourseProvider>(context, listen: false);
    mainPagingProvider = Provider.of(context, listen: false);
    mainPagingProvider.setPageController(pageController);
    mainPagingProvider.setCurrentPage(0);
    _checkEmailVerification();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    List<Widget> pages = <Widget>[
      HomeMain(),
      CourseMain(),
      TutorMain(),
      MyInfoMain(auth: widget.auth, onSignOut: widget.onSignOut,),
    ];


    return Selector<MainPagingProvider, String>(
      selector: (_, provider) => provider.currentPage.toString(),
      builder: (context, currentPage, child) => Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(56),
            child: AppBar(
              backgroundColor: Colors.white,
              elevation: 0,
              centerTitle: true,
              title: (currentPage=='0')
                  ?flavorLogoText()
                  :(currentPage=='1')
                  ?Center(
                    child: Text(
                      'Pick Your Course',
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xff333333),
                        fontWeight: FontWeight.w500,
                        fontFamily: 'NotoSansCJKkr'
                      ),
                    ),
                  )
                  :Text(
                    'Pick Your Tutor',
                    style: TextStyle(
                      fontSize: 16,
                      color: Color(0xff333333),
                      fontWeight: FontWeight.w500,
                      fontFamily: 'NotoSansCJKkr'
                  ),
              ),
              /*actions: [
                Center(
                  child: FlatButton(
                    child: Text('SignOut'),
                    onPressed: _signOut,
                  ),
                )
              ],*/
              leading: (currentPage!='0')?GestureDetector(
                onTap: (){
                  mainPagingProvider.jumpToPageByName('Home');
                },
                child: Container(
                  width: 50,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 30),
                    child: Image.asset(
                      'assets/images/intro/arrow_left.png',
                      width: 11.5,
                    ),
                  ),
                ),
              ):Container(),
            ),
          ),
          backgroundColor: Colors.white,
          //각각의 widget을 stack으로 보관 후 보여지는 순서만 바꿔줌
          body: SafeArea(
            child: PageView.builder(
              controller: mainPagingProvider.pageController,
              physics: NeverScrollableScrollPhysics(),
              //children: pages,
              itemBuilder: (context, index){
                return pages[index];
              },
              itemCount: pages.length,
            ),
          ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: int.parse(currentPage),
            type: BottomNavigationBarType.fixed,
            onTap: (int index){
              setState(() {
                currentTab = index;
                mainPagingProvider.setCurrentPage(index);
                mainPagingProvider.pageController.jumpToPage(index);
              });
            },
            //selectedItemColor: Colors.greenAccent,
            //unselectedItemColor: Colors.grey,
            items: [
              new BottomNavigationBarItem(
                    icon: SvgPicture.asset('assets/images/main/home.svg',
                    color: currentTab == 0? Colors.greenAccent : Colors.grey,),
                  title: Text('Home',
                    style: TextStyle(
                      color: currentTab == 0? Colors.greenAccent : Colors.grey,
                    ),
                  )
              ),
              new BottomNavigationBarItem(
                  icon: SvgPicture.asset('assets/images/main/book.svg',
                    color: currentTab == 1? Colors.greenAccent : Colors.grey,),
                  title: Text('Course',
                      style: TextStyle(
                        color: currentTab == 1? Colors.greenAccent : Colors.grey,
                      )
                  )
              ),
              new BottomNavigationBarItem(
                  icon: SvgPicture.asset('assets/images/main/speak.svg',
                    color: currentTab == 2? Colors.greenAccent : Colors.grey,),
                  title: Text('Tutor',
                      style: TextStyle(
                        color: currentTab == 2? Colors.greenAccent : Colors.grey,
                      )
                  )
              ),
              new BottomNavigationBarItem(
                  icon: SvgPicture.asset('assets/images/main/person.svg',
                    color: currentTab == 3? Colors.greenAccent : Colors.grey,),
                  title: Text('My Info',style: TextStyle(
                    color: currentTab == 3? Colors.greenAccent : Colors.grey,
                  )
                  )
              )
            ],
          )
      ),
    );
  }

  void _checkEmailVerification() async{
    _isEmailVerified = await widget.auth.isEmailVerified();
    if(!_isEmailVerified)
      _showVerifyEmailDialog();
  }

  void _showVerifyEmailDialog(){
    showDialog(context: context,
      builder: (BuildContext context){
        return AlertDialog(
          title: Text('Please verify your email'),
          content: Text('We need you verify email to continue use this app'),
          actions: [
            new FlatButton(onPressed: (){
              Navigator.of(context).pop();
              _sendVerifyEmail();
            }, child: Text('OK')),
          ],
        );
      }
    );
  }

  flavorLogoText(){
    return Text('Flavor',
      style: TextStyle(
          fontSize: 28,
          color: Color(0xFF27d794),
          fontFamily: 'Montserrat',
          fontWeight: FontWeight.w800
      ),
    );
  }

  void _sendVerifyEmail(){
    widget.auth.sendEmailVerification();
    _signOut();
  }


  void _signOut() async{
    try{
      await widget.auth.signOut();
      widget.onSignOut();
    }catch(e){
      print(e);
    }
  }

  customSnackBar(String text){
    return SnackBar(
      elevation: 0,
      behavior: SnackBarBehavior.floating,
      duration: Duration(seconds: 1),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16)
      ),
      content: Padding(
        padding: EdgeInsets.only(left: 6),
        child: Text(
            'Pick my course $text'
        ),
      ),
    );
  }
}
