import 'package:flutter/material.dart';
import 'package:heystars_app/providers/course_provider.dart';
import 'package:heystars_app/screens/component/button/startBtn.dart';
import 'package:heystars_app/screens/component/cards/tutor/tutor_slider.dart';
import 'package:heystars_app/screens/layouts/chatbot_page/study_main.dart';
import 'package:heystars_app/services/common/popup.dart';
import 'package:provider/provider.dart';

class TutorMain extends StatefulWidget {
  @override
  _TutorMainState createState() => _TutorMainState();
}

class _TutorMainState extends State<TutorMain> {
  bool isIdol;
  CourseProvider courseProvider;

  @override
  void initState() {
    super.initState();
    isIdol = true;
    courseProvider = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return SizedBox.expand(
      child: SingleChildScrollView(
        child: Column(
          children: [
            idolOrInfluencer(),
            TutorSlider(isHomePage: false,),
            SizedBox(height: size.height*0.08,),
            StartBtn(function: (){
              if(courseProvider.selectedChapter!=null && courseProvider.selectedTutor!=null){
                Navigator.push(context, MaterialPageRoute(builder: (context) => StudyChoose()));
              }else{
                Popup().chatbotStartPopup(context, size);
              }
            },)
          ],
        ),
      ),
    );
  }

  idolOrInfluencer(){
    return Padding(
      padding: EdgeInsets.fromLTRB(24, 12, 24, 12),
      child: Row(
        children: [
          GestureDetector(
            onTap: (){
              setState(() {
                isIdol = true;
              });
            },
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(17),
                color: isIdol
                    ?Color(0xff27d794)
                    :Colors.transparent,
                border: Border.all(
                  color: isIdol
                      ?Colors.transparent
                      :Color(0xff666666)
                )
              ),
              child: Padding(
                padding: EdgeInsets.fromLTRB(12, 8, 12, 8),
                child: Text(
                  'IDOL',
                  style: TextStyle(
                    color: isIdol
                        ?Colors.white
                        :Color(0xff666666),
                    fontSize: 12,
                    fontFamily: 'NotoSansCJKkr',
                    fontWeight: FontWeight.w500
                  ),
                ),
              ),
            ),
          ),

          SizedBox(
            width: 8,
          ),

          GestureDetector(
            onTap: (){
              setState(() {
                isIdol = false;
              });
            },
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(17),
                color: isIdol
                    ?Colors.transparent
                    :Color(0xff27d794),
                border: Border.all(
                  color: isIdol
                      ?Color(0xff666666)
                      :Colors.transparent
                )
              ),
              child: Padding(
                padding: EdgeInsets.fromLTRB(12, 8, 12, 8),
                child: Text(
                  'INFLUENCER',
                  style: TextStyle(
                    color: isIdol
                        ?Color(0xff666666)
                        :Colors.white,
                    fontSize: 12,
                    fontFamily: 'NotoSansCJKkr',
                    fontWeight: FontWeight.w500
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
