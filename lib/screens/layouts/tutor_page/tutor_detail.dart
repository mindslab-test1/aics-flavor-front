import 'package:flutter/material.dart';
import 'package:heystars_app/providers/course_provider.dart';
import 'package:provider/provider.dart';

class TutorDetail extends StatelessWidget {

  final int tutorNum;

  TutorDetail({
    this.tutorNum
  });

  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;
    //TutorProvider tutorProvider = Provider.of(context, listen: false);
    CourseProvider courseProvider = Provider.of(context, listen: false);

    return Scaffold(
      body: SizedBox.expand(
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                //height: size.height*0.672,
                child: Image.network(
                  //tutorProvider.tutorImg[tutorNum],
                  courseProvider.tutorParser.tutors[tutorNum].imgUrl,
                  fit: BoxFit.cover,
                  width: size.width,
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Colors.white,
                  border: Border.all(
                    color: Colors.white
                  )
                ),
                height: size.height*0.354,
                width: size.width,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(24, 24, 24, 0),
                  child: Column(
                    children: [
                      Text(
                        //tutorProvider.tutorJob[tutorNum],
                        courseProvider.tutorParser.tutors[tutorNum].job,
                        style: TextStyle(
                          fontSize: 12,
                          fontFamily: 'NotoSansCJKkr',
                          fontWeight: FontWeight.w500,
                          color: Color(0xff333333)
                        ),
                      ),
                      SizedBox(height: 5,),
                      Text(
                        //tutorProvider.tutorName[tutorNum],
                        courseProvider.tutorParser.tutors[tutorNum].name,
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'NotoSansCJKkr',
                          fontWeight: FontWeight.w700,
                          color: Color(0xff333333)
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 3, bottom: 8),
                        child: Text(
                          //tutorProvider.tutorGroup[tutorNum],
                          courseProvider.tutorParser.tutors[tutorNum].member,
                          style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'NotoSansCJKkr',
                            fontWeight: FontWeight.w500,
                            color: Color(0xff333333)
                          ),
                        ),
                      ),
                      Text(
                        //tutorProvider.tutorDetail[tutorNum],
                        courseProvider.tutorParser.tutors[tutorNum].description,
                        style: TextStyle(
                          fontSize: 12,
                          fontFamily: 'NotoSansCJKkr',
                          fontWeight: FontWeight.w300,
                          color: Color(0xff666666)
                        ),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 22,),
                      Container(
                        width: size.width*0.417,
                        child: RaisedButton(
                          onPressed: (){},
                          color: Colors.white,
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(24),
                            side: BorderSide(color: Color(0xff27d794))
                          ),
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(0, 13, 0, 13),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Subscribe',
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: Color(0xff27d794),
                                    fontFamily: 'NotoSansCJKkr',
                                    fontWeight: FontWeight.w300
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: size.width*0.042, top: size.height*0.067),
              child: Align(
                alignment: Alignment.topLeft,
                child: GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Container(
                    width: 45,
                    height: 45,
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(255, 255, 255, 0.7),
                        borderRadius: BorderRadius.circular(4)
                    ),
                    child: Center(
                      child: Icon(
                          Icons.arrow_back_ios
                      ),
                    ),
                  ),
                ),
              )
            )
          ],
        ),
      ),
    );
  }
}
