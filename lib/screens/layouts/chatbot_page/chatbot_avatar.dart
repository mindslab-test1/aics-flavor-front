import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:heystars_app/providers/scenario_list_status.dart';
import 'package:heystars_app/providers/user_provider.dart';
import 'package:heystars_app/screens/component/button/chatbot_select_btn.dart';
import 'package:heystars_app/screens/component/cards/chatbot/ai_avatar_chat.dart';
import 'package:heystars_app/services/fade_edge_scrollview/fading_edge_scrollview.dart';
import 'package:heystars_app/services/json_parser/chatbot/chatbot_json.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';

class ChatbotAvatar extends StatefulWidget {
  final bool isStudy;
  final bool isFinished;

  ChatbotAvatar({
    this.isStudy,
    this.isFinished
  });

  @override
  _ChatbotAvatarState createState() => _ChatbotAvatarState();
}

class _ChatbotAvatarState extends State<ChatbotAvatar> with ChangeNotifier{
  String _apiServer = DotEnv().env['API_SERVER'];

  List<Widget> _chatList = List();

  VideoPlayerController _controller1;
  VideoPlayerController _controller2;
  ChatbotJson chatbotJson;
  int index = 0;
  bool isLoading;
  bool allAvatarIsOver;

  int currentVideo = 1;
  int currentStep;
  int studentChoiceBtn;
  List<IntroScenario> currentIntroScenario;
  ScenarioListStatus scenarioListStatus;
  UserProvider userProvider;

  final _scrollController = ScrollController();

  @override
  void dispose() {
    _controller1.dispose();
    _controller2.dispose();
    super.dispose();
  }


  Size cardSize;
  Offset cardPosition;

  
  @override
  void initState() {
    super.initState();

    scenarioListStatus = Provider.of(context, listen: false);
    userProvider = Provider.of(context, listen: false);

    setState(() {
      allAvatarIsOver = false;
      isLoading = true;
    });
    getScenario();

  }

  String getOtherLangAnswer(){
    if(userProvider.lang == 'Indo') return currentIntroScenario[0].comModelAnswers[index].indoText;
    else if(userProvider.lang == 'Viet') return currentIntroScenario[0].comModelAnswers[index].vietTExt;
    else return currentIntroScenario[0].comModelAnswers[index].engText;
  }

  startAiAvatar(List<IntroScenario> introScenario){
    setState(() {
      currentIntroScenario = introScenario;
    });

    _controller1 = VideoPlayerController.network(currentIntroScenario[0].comModelAnswers[index].mediaModel.url)..initialize()..addListener(videoListener1);
    _controller1.play();

    setState(() {
      _chatList.add(AiChatMessage(currentStep, index,
          currentIntroScenario[0].comModelAnswers[index].korText,
          getOtherLangAnswer(), playVideo));
      index = index + 1;
    });

    _controller2 = VideoPlayerController.network(currentIntroScenario[0].comModelAnswers[index].mediaModel.url)
      ..initialize()..addListener(videoListener2);
  }

  void videoListener1(){

    if(_controller1.value.position == _controller1.value.duration && index<=currentIntroScenario[0].comModelAnswers.length-1){
      final old = _controller1;

      setState(() {
        currentVideo=2;
        _controller2.play();

        _chatList.add(AiChatMessage(currentStep, index,
            currentIntroScenario[0].comModelAnswers[index].korText,
            getOtherLangAnswer(), playVideo));

        index = index+1;
      });

      if(index<currentIntroScenario[0].comModelAnswers.length) {
        final controller = VideoPlayerController.network(currentIntroScenario[0].comModelAnswers[index].mediaModel.url);

        if (old != null) {
          old.removeListener(videoListener1);
          old.pause();
        }

        _controller1 = controller;
        controller.initialize().then((_) {
          old?.dispose();
          controller.addListener(videoListener1);
        });
      }else{
        setState(() {
          allAvatarIsOver = true;
        });
        startNextStep();
      }
    }
  }

  void lastChatListener(){
    if(currentVideo == 1){
      if(_controller1.value.position == _controller1.value.duration){
        Navigator.pop(context, false);
      }
    }else{
      if(_controller2.value.position == _controller2.value.duration){
        Navigator.pop(context, false);
      }
    }
  }

  void videoListener2(){

    if(_controller2.value.position == _controller2.value.duration && index<=currentIntroScenario[0].comModelAnswers.length-1){
      final old = _controller2;

      setState(() {
        currentVideo = 1;
        _controller1.play();

        _chatList.add(AiChatMessage(currentStep, index,
            currentIntroScenario[0].comModelAnswers[index].korText,
            getOtherLangAnswer(), playVideo));

        index = index+1;
      });

      if(index<currentIntroScenario[0].comModelAnswers.length) {
        final controller = VideoPlayerController.network(
            currentIntroScenario[0].comModelAnswers[index].mediaModel.url);

        if (old != null) {
          old.removeListener(videoListener2);
          old.pause();
        }

        _controller2 = controller;
        controller.initialize().then((_) {
          old?.dispose();
          controller.addListener(videoListener2);
        });
      }else{
        setState(() {
          allAvatarIsOver = true;
        });
        startNextStep();
      }
    }
  }

  playVideo(int currentStep, int currentIndex, String url, bool isLastChat){
    if(allAvatarIsOver){
      VideoPlayerController controller;
      if(url == null) controller = VideoPlayerController.network(currentIntroScenario[0].comModelAnswers[currentIndex].mediaModel.url);
      else controller = VideoPlayerController.network(url);

      if(currentVideo == 1) {
        final old = _controller2;

        if (old != null) {
          old.removeListener(videoListener1);
          old.pause();
        }

        _controller2 = controller;
        controller.initialize().then((_){
          old?.dispose();
          setState(() {
            currentVideo = 2;
          });
          if(isLastChat) {
            setState(() {
              allAvatarIsOver = false;
            });
            controller.addListener(lastChatListener);
          }
          controller.play();
        });

      }else if(currentVideo == 2){
        final old = _controller1;

        if(old != null){
          old.removeListener(videoListener2);
          old.pause();
        }

        _controller1 = controller;
        controller.initialize().then((_){
          old?.dispose();
          setState(() {
            currentVideo = 1;
          });
          if(isLastChat) {
            setState(() {
              allAvatarIsOver = false;
            });
            controller.addListener(lastChatListener);
          }
          controller.play();
        });
      }
    }
  }

  startNextStep(){

    setState(() {
      currentStep += 2;
    });

    if(currentIntroScenario[currentStep-1].answer.contains('S')){
      setState(() {
        _chatList.add(
            SelectAvatarAnswerBtn(
              selectedBtn: studentChoiceBtn,
              studentChoice: studentChoice,
              answer1: (currentStep-1<currentIntroScenario.length)?currentIntroScenario[currentStep-1]:null,
              answer2: (currentStep<currentIntroScenario.length)?currentIntroScenario[currentStep]:null,
              addChatList: addChatList,
              playVideo: playVideo,
            )
        );
      });
    }
  }

  studentChoice(int index){
    setState(() {
      if(studentChoiceBtn != null){
        studentChoiceBtn = index;
      }
    });
  }

  getScenario() async{
    setState(() {
      isLoading = false;
      currentStep = 0;
    });

    if(widget.isFinished == true){
      startAiAvatar(scenarioListStatus.introScenariosListFinished);
    }else if(widget.isStudy == true){
      startAiAvatar(scenarioListStatus.introScenariosListStudy);
    }else if(widget.isStudy == false){
      startAiAvatar(scenarioListStatus.introScenariosListQuiz);
    }
  }
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: () {
        int count = 0;
        Navigator.of(context).popUntil((_) => count++ >= 2);
        return;
      },
      child: Scaffold(
        body: isLoading?Container():Container(
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  color: Colors.black,
                  height: 243,
                  width: size.width,
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 240,
                  child: AspectRatio(
                    //aspectRatio: (currentVideo == 1)? _controller1.value.aspectRatio:_controller2.value.aspectRatio,
                    aspectRatio: 0.78,
                    child: VideoPlayer(
                        (currentVideo == 1)?
                        _controller1
                            :_controller2
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: size.height-245,
                  child: ListView.builder(
                    reverse: true,
                    controller: _scrollController,
                    itemBuilder: (context, index) => Container(
                      child: _chatList[(_chatList.length-1)-index],
                    ),
                    itemCount: _chatList.length,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(24, 32, 0, 0),
                  child: GestureDetector(
                    onTap: (){
                      int count = 0;
                      Navigator.of(context).popUntil((_) => count++ >= 2);
                    },
                    child: Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(255, 255, 255, 0.7),
                        borderRadius: BorderRadius.all(Radius.circular(4))
                      ),
                      child: Padding(
                        padding: EdgeInsets.only(left: 7),
                        child: Icon(Icons.arrow_back_ios),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  addChatList(Widget chatMessage){
    setState(() {
      _chatList.add(chatMessage);
    });
  }

}
