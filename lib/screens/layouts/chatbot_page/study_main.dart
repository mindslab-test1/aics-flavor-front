import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:heystars_app/providers/course_provider.dart';
import 'package:heystars_app/providers/scenario_list_status.dart';
import 'package:heystars_app/screens/layouts/chatbot_page/message.dart';
import 'package:heystars_app/screens/layouts/chatbot_page/slideRightRoute.dart';
import 'package:heystars_app/services/audio_recorder/recorder.dart';
import 'package:heystars_app/services/json_parser/chatbot/chatbot_json.dart';
import 'package:heystars_app/services/repositories/course_repository.dart';
import 'package:provider/provider.dart';
import '../../../providers/user_provider.dart';
import 'chatbot_avatar.dart';
import 'package:file/local.dart';

class StudyChoose extends StatefulWidget {
  final String title;

  const StudyChoose({Key key, this.title,}) : super(key: key);

  @override
  _StudyChooseState createState() => _StudyChooseState();
}

class _StudyChooseState extends State<StudyChoose> {
  final List<Message> _messages = List();
  final _controller= ScrollController();
  ScenarioListStatus scenarioListStatus;
  ChatbotJson chatbotJson;
  UserProvider userProvider;
  CourseProvider courseProvider;
  String scenarioName;
  bool isLoading;
  Recorder recorder;
  LocalFileSystem localFileSystem;


  @override
  void initState() {
    super.initState();
    userProvider = Provider.of<UserProvider>(context, listen: false);
    courseProvider = Provider.of<CourseProvider>(context, listen: false);
    getData();
    scenarioListStatus = Provider.of<ScenarioListStatus>(context, listen: false);
    isLoading = true;
    localFileSystem = LocalFileSystem();
    recorder = Recorder(localFileSystem: localFileSystem);
  }

  @override
  void dispose(){
    _controller.dispose();
    super.dispose();
  }

  // study, quiz에서 질문 부분
  seeMessage(){
    // 퀴즈까지 다 끝났을 경우
    if(scenarioListStatus.index == scenarioListStatus.scenarioList.length){
      return finishVideo();
    }else{
      _messages.add(Message(direction: 'left', type: 'question', msg: scenarioListStatus.scenarioList [scenarioListStatus.index][0].answerKor,
          pron: scenarioListStatus.scenarioList[scenarioListStatus.index][0].answer,scenarioName: scenarioName,
          mediaUrl: scenarioListStatus.scenarioList[scenarioListStatus.index][0].mediaUrl));
    }
    if(scenarioName=='study'){
      _messages.add(Message(direction: 'left',type: 'image', msg: scenarioListStatus.scenarioList[scenarioListStatus.index][0].mediaUrl,
        pron: '',));
      // loadImg();
    }else{
      _messages.add(Message(direction: 'left', type: 'speak', msg: scenarioListStatus.scenarioList[scenarioListStatus.index][0].question,));
      // makeBtn();
    }
    Future.delayed(Duration(seconds: 2), (){
      setState(() {
        makeBtn();
      });
    });
    // makeBtn();
  }


  // 다 끝나고 마무리 영상
  finishVideo(){
    Future.delayed(const Duration(seconds: 4), (){
      Navigator.push(context,
          SlideRightRoute(page : ChatbotAvatar(isStudy: false, isFinished: true,))
      ).then((value){
        if(value ==true){
          Navigator.pop(context);
        }
      });
    });
  }


  // 데이터를 가져오는 부분
  Future<void> getData() async {
    final response = await CourseRepository().getScenarioAll(courseProvider.chaptersParser.chapters[courseProvider.selectedChapter].titleEng, userProvider.lang);
    var formatted = utf8.decode(response.bodyBytes);
    chatbotJson = ChatbotJson.fromJson(json.decode(formatted));

    scenarioListStatus.setIntroScenariosListStudy(chatbotJson.introScenariosListStudy);
    scenarioListStatus.setIntroScenariosListQuiz(chatbotJson.introScenariosListQuiz);
    scenarioListStatus.setIntroScenariosListFinished(chatbotJson.introScenariosListFinished);
    scenarioListStatus.setStudyScenarioListStudy(chatbotJson.studyScenarioListStudy);
    scenarioListStatus.setStudyScenarioListRead(chatbotJson.studyScenarioListRead);
    scenarioListStatus.setQuizScenarioList(chatbotJson.quizScenarioList);
    scenarioListStatus.setScenarioList('study');
    scenarioListStatus.setInitIndex(0);
    scenarioName = 'study';

    // 시작 영상을 띄우는 부분
    Navigator.push(context,
        SlideRightRoute(page: ChatbotAvatar(isStudy: true, isFinished: false,))
    ).then((value){
      if(value ==false){
        Navigator.pop(context);
      }else{
        setState(() {
          isLoading = false;
          _messages.add(
              Message(direction: 'center', msg: '시작합니다.', pron: 'start!!',));
          seeMessage();
        });
      }
    });

  }

  // 사용자의 답변 부분
  addMessage(String msg, String pron,) {
    setState(() {
      _messages.add(Message(direction: 'right', msg: msg, pron: pron, scenarioName: scenarioName,));
    });
  }

  // 사용자의 답변에 따라 질문을 다르게하는 부분
  responseMessage(String msg, String pron, String mediaUrl, bool check){
    setState(() {
      // 문제를 맞게 풀었을 때
      if (check ==true){
        _messages.add(Message(direction: 'left', type: 'question', msg:msg, pron: pron, mediaUrl: mediaUrl,));
        scenarioListStatus.setIndex(1);
         if (scenarioListStatus.index <= scenarioListStatus.scenarioList.length && scenarioName =='study'){
           _messages.add(Message(direction: 'left', type: 'speak', msg: scenarioListStatus.studyScenarioListRead[scenarioListStatus.index-1][0].answerKor,
             pron: scenarioListStatus.studyScenarioListRead[scenarioListStatus.index-1][0].answer,));
           _messages.add(Message(direction: 'left', type: 'mic', msg: msg, pron: pron, addMessage: addMessage, checkMessage: checkMessage,));
         }else {
           seeMessage();
         }
       // 문제 틀림
      }else{
        _messages.add(Message(direction: 'left', type: 'question', msg:msg, pron: pron,));
        makeBtn();
      }
    });
  }

  // study에서 quiz로 시나리오 변경
  changeScenario(){
    if (scenarioName == 'study') {
      scenarioListStatus.setIndex(-scenarioListStatus.index);
      // quiz 진행을 물어보고 영상 띄움
      Future.delayed(const Duration(seconds: 4), (){
        Navigator.push(context,
            SlideRightRoute(page : ChatbotAvatar(isStudy: false, isFinished: false,))
        ).then((value){
          if(value ==true){
            setState(() {
              scenarioName = 'quiz';
              scenarioListStatus.setScenarioList('quiz');
              seeMessage();
            });
          }else{
            Navigator.pop(context);
          }
        });
      });
    }
  }

  // 발음으로 읽는 부분에 대한 분기
  checkMessage(String word){
    setState(() {
      if (word =='Reading'){
        makeSpeakResponse(1);
      }else if (word =='W'){
       makeSpeakResponse(3);
      }else{
        makeSpeakResponse(2);
      }
    });
  }

  // 발음의 정답여부에 따라 질문이 달라지는 부분
  makeSpeakResponse(int responIdx){
    _messages.add(Message(direction: 'left', type: 'question', msg: scenarioListStatus.studyScenarioListRead[scenarioListStatus.index-1][responIdx].responseKor,
      pron:scenarioListStatus.studyScenarioListRead[scenarioListStatus.index-1][responIdx].response));
    if (responIdx ==1){
      if(scenarioListStatus.index >= scenarioListStatus.scenarioList.length){
        changeScenario();
      }else{
        seeMessage();
      }
    }else{
    _messages.add(Message(direction: 'left', type: 'mic', addMessage: addMessage, checkMessage: checkMessage,));
    }
  }


  // 보기를 만드는 부분
  makeBtn(){
    print('make btn start');
    _messages.add(
            Message(
              direction: 'left',type: 'choices', choices: [
              scenarioListStatus.scenarioList[scenarioListStatus.index][1].answerKor,
              scenarioListStatus.scenarioList[scenarioListStatus.index][2].answerKor,
              scenarioListStatus.scenarioList[scenarioListStatus.index][3].answerKor,
              scenarioListStatus.scenarioList[scenarioListStatus.index][4].answerKor,
            ], choicesProns: [
              scenarioListStatus.scenarioList[scenarioListStatus.index][1].answer,
              scenarioListStatus.scenarioList[scenarioListStatus.index][2].answer,
              scenarioListStatus.scenarioList[scenarioListStatus.index][3].answer,
              scenarioListStatus.scenarioList[scenarioListStatus.index][4].answer,
            ],
              addMessage: addMessage,
              responseMessage: responseMessage,
          scenarioName: scenarioName,
        ));
  }



  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    final screenHeight = MediaQuery
        .of(context)
        .size
        .height;


    Timer(Duration(seconds: 1),
        ()=>{if(_controller.hasClients){
          _controller.animateTo(_controller.position.maxScrollExtent, duration: Duration(seconds: 1), curve: Curves.fastOutSlowIn)
        }});


    //문제에서 보기가 끝까지 올라오도록 한번 더 스크롤
   // if(_controller.hasClients){
   //   Future.delayed(Duration(seconds: 1), (){
   //     Timer(Duration(seconds: 1),(){
   //       _controller.animateTo(_controller.position.maxScrollExtent, duration: Duration(seconds: 1), curve: Curves.fastOutSlowIn);
   //     });
   //   });
   // }

    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          centerTitle: true,
          title: Text(''),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              padding: const EdgeInsets.only(left: 30.2),
              child: Image.asset('assets/images/intro/arrow_left.png',
                width: 11.5,),
            ),
          )
      ),
      body: Container(
          color: Colors.white,
          height: screenHeight,
          width: screenWidth,
          child: Container(
            child: isLoading?Center(
              child: CircularProgressIndicator(),
            ):Column(
              children: [
                Flexible(
                  child: ListView.builder(
                    controller: _controller,
                    itemCount: _messages.length,
                    itemBuilder: (_, int idx) => _messages[idx],
                  ),
                ),
              ],
            ),
          )
      ),
    );
  }



}