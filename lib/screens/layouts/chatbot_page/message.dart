import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:heystars_app/screens/component/cards/chatbot/question_container.dart';
import 'package:heystars_app/screens/component/cards/chatbot/question_image_container.dart';
import 'package:heystars_app/screens/component/cards/chatbot/selected_btn_card.dart';
import 'package:heystars_app/screens/component/cards/chatbot/speaking_container.dart';
import 'package:heystars_app/screens/component/cards/chatbot/speaking_record_container.dart';
import 'package:heystars_app/services/common/font.dart';

class Message extends StatefulWidget {

  final String type;
  final String msg;
  final String pron;
  final String direction;
  final List<String> choices;
  final List<String> choicesProns;
  final String mediaUrl;
  final Function addMessage;
  final Function responseMessage;
  final Function checkMessage;
  final String scenarioName;

  Message({Key key, this.type, this.msg, this.pron, this.direction, this.choices, this.choicesProns, this.addMessage, this.responseMessage, this.checkMessage, this.mediaUrl, this.scenarioName, }) : super(key: key);

  @override
  _MessageState createState() => _MessageState();
}

class _MessageState extends State<Message> with AutomaticKeepAliveClientMixin {

  // study 시작 알림 메세지
  Widget centerSpeech(){
    final screenWidth = MediaQuery.of(context).size.width;
    return Center(
      child: Container(
        width: screenWidth * 0.222,
        padding: EdgeInsets.only(top: 8, bottom: 8),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(36.0),
            color: const Color.fromRGBO(232, 255, 246, 1)
        ),
        child: Center(
          child: Column(
            children: [
              Text(widget.msg,
                style: msgStyle.copyWith(fontSize: 10),),
              Text(widget.pron,
                style: pronStyle.copyWith(fontSize: 10),)
            ],
          ),
        ),
      ),
    );
  }

  // tutor 말풍선
  Widget leftSpeech(){
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if(widget.type=='question') QuestionContainer(question: widget.msg, questionPron: widget.pron, mediaUrl: widget.mediaUrl, scenarioName: widget.scenarioName,),
          if(widget.type =='speak') SpeakingContainer(speak: widget.msg, speakPron: widget.pron,),
          if(widget.type =='mic') SpeakingRecordContainer(addMessage: widget.addMessage, checkMessage: widget.checkMessage,),
          if(widget.type == 'image') QuestionImageContainer(path: widget.msg),
          if(widget.type =='choices') SelectedBtnCard(choices: widget.choices, choicesPron: widget.choicesProns,addMessage: widget.addMessage, responseMessage: widget.responseMessage, scenarioName: widget.scenarioName, )
    ],
      ),
    );
  }

  // user 응답말풍선
  Widget rightSpeech(){
    final screenWidth = MediaQuery.of(context).size.width;
    return Container(
        child: Container(
          margin: EdgeInsets.only(top: 8, left: screenWidth * 0.178, bottom: 10),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                constraints: BoxConstraints(maxWidth: 200),
                padding: EdgeInsets.only(
                    top: 24, left: 24, right: 24, bottom: 16),
                decoration: BoxDecoration(
                    color: const Color.fromRGBO(39, 215, 148, 1),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(100.0),
                        bottomLeft: Radius.circular(100.0),
                        bottomRight: Radius.circular(100.0))),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(widget.msg,
                      style: msgStyle.copyWith(color: Colors.white),
                      softWrap: true,),
                    // widget.scenarioName == 'study'?
                    if(widget.scenarioName =='study')SizedBox(height: 4),
                    if(widget.scenarioName == 'study' &&widget.pron !='')
                    Text(widget.pron,
                      style: pronStyle.copyWith(color: Colors.white),
                      textAlign: TextAlign.right,)
                  ],
                ),
              ),
              SizedBox(
                width: 8,
              ),
              Container(
                  margin: EdgeInsets.only(right: screenWidth * 0.067),
                  child: CircleAvatar(
                    backgroundColor: Colors.transparent,
                    child: SvgPicture.asset('assets/images/chatbot/profile_empty.svg'),
                  )
              )
            ],
          ),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    final screenWidth = MediaQuery.of(context).size.width;
    return Container(
      width: screenWidth,
      child: widget.direction == 'center'? centerSpeech()
      :widget.direction =='left'? leftSpeech():rightSpeech());
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

}
