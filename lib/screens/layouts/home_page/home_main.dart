import 'dart:async';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:heystars_app/providers/course_provider.dart';
import 'package:heystars_app/providers/main_paging_provider.dart';
import 'package:heystars_app/screens/component/cards/course/course_card.dart';
import 'package:heystars_app/screens/component/cards/home/home_course.dart';
import 'package:heystars_app/screens/component/cards/tutor/tutor_slider.dart';
import 'package:heystars_app/services/common/common.dart';
import 'package:heystars_app/services/json_parser/course/chapters_parser.dart';
import 'package:heystars_app/services/json_parser/course/tutors_parser.dart';
import 'package:heystars_app/services/repositories/course_repository.dart';
import 'package:provider/provider.dart';


class HomeMain extends StatefulWidget {

  @override
  _HomeMainState createState() => _HomeMainState();
}

class _HomeMainState extends State<HomeMain> {

  CourseProvider courseProvider;
  MainPagingProvider mainPagingProvider;
  List<Widget> cardList;
  bool isgetData;

  @override
  void initState() {
    super.initState();
    isgetData = false;
    courseProvider = Provider.of(context, listen: false);
    mainPagingProvider = Provider.of(context, listen: false);
  }

  Future<bool> _getTutorsAndChapters() async{
    Map tutorsAndChapter = await CourseRepository().getTutorsAndChapters();
    TutorsParser tutorsParser = TutorsParser.fromJson(tutorsAndChapter);
    ChaptersParser chaptersParser = ChaptersParser.fromJson(tutorsAndChapter);

    setState(() {
      courseProvider.setChaptersParser(chaptersParser);
      courseProvider.setTutorsParser(tutorsParser);
      isgetData = true;
    });

    return true;
  }

  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;

    if(!isgetData) {
      return SizedBox.expand(
        child: FutureBuilder<bool>(
          future: _getTutorsAndChapters(),
          builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
            if (snapshot.hasData) {

              return Container();
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      );
    }else{
      return SizedBox.expand(
        child: CustomScrollView(
          slivers: [
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  Column(
                    children: [
                      //flavorLogoText(),
                      //SizedBox(height: 22,),
                      //saleBox(size),
                      movePickpage('Pick Your Tutor', size, 'Tutor'),
                      TutorSlider(isHomePage: true, movePickPage: movePickPage,),
                      movePickpage('Course', size, 'Course'),
                    ],
                  )
                ],
              ),
            ),
            HomeCourse(movePickPage: movePickPage,),
          ],
        )
      );
    }
  }

  flavorLogoText(){
    return Text('Flavor',
      style: TextStyle(
          fontSize: 28,
          color: Color(0xFF27d794),
          fontFamily: 'Montserrat',
          fontWeight: FontWeight.w800
      ),
    );
  }

  saleBox(Size size){
    return Container(
      width: size.width*0.867,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Color(0xfff2f2f2)
      ),
      child: Align(
        alignment: Alignment.centerRight,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(24, 10, 24, 10),
          child: (
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Only, 5days CHANCE!',
                    style: TextStyle(
                        fontFamily: 'NotoSansCJKk',
                        fontWeight: FontWeight.w700,
                        fontSize: 14
                    ),
                  ),
                  Text('IPAD FLEX',
                    style: TextStyle(
                        fontFamily: 'NotoSansCJKk',
                        fontWeight: FontWeight.w300,
                        fontSize: 12
                    ),
                  )
                ],
              )
          ),
        ),
      ),
    );
  }

  movePickpage(String text, Size size, String movePage){
    return Padding(

      padding: EdgeInsets.fromLTRB(0, 20, 0, 16),
      child: Center(
        child: InkWell(
          onTap: (){
            movePickPage(movePage);
          },
          child: Ink(
            child: Container(
              width: size.width*0.867,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    text,
                    style: TextStyle(
                      fontSize: 18,
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w600,
                      color: Color(0xFF262727)
                    ),
                  ),
                  Icon(
                    Icons.keyboard_arrow_right,
                    size: 25,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void movePickPage(String movePage){
    if(movePage == 'Tutor'){
      if(courseProvider.selectedTutor != null){
        Scaffold.of(context).showSnackBar(customSnackBar(courseProvider.tutorParser.tutors[courseProvider.selectedTutor].name));
      }
    }else if(movePage == 'Course'){
      if(courseProvider.selectedChapter != null)
        Scaffold.of(context).showSnackBar(customSnackBar(courseProvider.chaptersParser.chapters[courseProvider.selectedChapter].titleKor));
    }
    setState(() {
      mainPagingProvider.jumpToPageByName(movePage);
    });
  }

  customSnackBar(String text){
    return SnackBar(
      elevation: 0,
      behavior: SnackBarBehavior.floating,
      duration: Duration(seconds: 1),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16)
      ),
      content: Padding(
        padding: EdgeInsets.only(left: 6),
        child: Text(
            'Pick my course $text'
        ),
      ),
    );
  }
}
