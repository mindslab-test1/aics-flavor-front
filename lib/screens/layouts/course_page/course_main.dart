import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:heystars_app/providers/course_provider.dart';
import 'package:heystars_app/screens/component/button/startBtn.dart';
import 'package:heystars_app/screens/component/cards/course/course_card.dart';
import 'package:heystars_app/screens/component/cards/course/course_grid.dart';
import 'package:heystars_app/screens/component/cards/home/home_course.dart';
//import 'package:heystars_app/screens/layouts/chatbot_page/aiChatTest.dart';
import 'package:heystars_app/screens/layouts/chatbot_page/chatbot_avatar.dart';
import 'package:heystars_app/screens/layouts/chatbot_page/study_main.dart';
import 'package:heystars_app/services/common/popup.dart';
import 'package:provider/provider.dart';

class CourseMain extends StatefulWidget {
  @override
  _CourseMainState createState() => _CourseMainState();
}

class _CourseMainState extends State<CourseMain> {
  CourseProvider courseProvider;
  bool isSelectAll;
  
  @override
  void initState() {
    super.initState();
    isSelectAll = true;
    courseProvider = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return SizedBox.expand(
      child: Padding(
        padding: EdgeInsets.only(left: 24, right: 24),
        child: Container(
          child: CustomScrollView(
            slivers: [
              sliverList(),
              SliverGridCard(size: size,),
              startBtn(size)
            ],
          ),
        ),
      ),
    );
  }

  startBtn(Size size){
    return SliverPadding(
      padding: EdgeInsets.only(top: 34, bottom: 50),
      sliver: SliverList(
        delegate: SliverChildListDelegate(
          [
            Center(
              child: StartBtn(
                function: (){
                  if(courseProvider.selectedTutor!=null && courseProvider.selectedChapter!=null) {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => StudyChoose()));
                  }else{
                    Popup().chatbotStartPopup(context, size);
                  }
                },
              ),
            )
          ]
        ),
      ),
    );
  }

  sliverList(){
    return SliverList(
      delegate: SliverChildListDelegate(
          [
            Padding(
              padding: EdgeInsets.fromLTRB(0, 16, 0, 16),
              child: Text(
                'Pick Your Course',
                style: TextStyle(
                    fontSize: 18,
                    color: Color(0xff333333),
                    fontFamily: 'Montserrat',
                    fontWeight: FontWeight.w600
                ),
              ),
            ),

            allOrFinished()
          ]
      ),
    );
  }

  allOrFinished(){
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 12, 0, 12),
      child: Row(
        children: [
          GestureDetector(
            onTap: (){
              setState(() {
                isSelectAll = true;
              });
            },
            child: Container(
              decoration: BoxDecoration(
                  color: isSelectAll
                      ?Color(0xff27d794)
                      :Colors.transparent,
                  border: Border.all(
                    color: isSelectAll
                        ?Colors.transparent
                        :Color(0xff666666),
                  ),
                  borderRadius: BorderRadius.circular(17)
              ),
              child: Padding(
                padding: EdgeInsets.fromLTRB(12, 8, 12, 8),
                child: Text(
                  'ALL',
                  style: TextStyle(
                      fontSize: 12,
                      color: isSelectAll
                          ?Colors.white
                          :Color(0xff666666),
                      fontFamily: 'NotoSansCJKkr',
                      fontWeight: FontWeight.w500
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            width: 8,
          ),
          GestureDetector(
            onTap: (){
              setState(() {
                isSelectAll = false;
              });
            },
            child: Container(
              decoration: BoxDecoration(
                  color: isSelectAll
                      ?Colors.transparent
                      :Color(0xff27d794),
                  border: Border.all(
                      color: isSelectAll
                          ?Color(0xff666666)
                          :Colors.transparent
                  ),
                  borderRadius: BorderRadius.circular(17)
              ),
              child: Padding(
                padding: EdgeInsets.fromLTRB(12, 8, 12, 8),
                child: Text(
                  'FINISHED',
                  style: TextStyle(
                      color: isSelectAll
                          ?Color(0xff666666)
                          :Colors.white,
                      fontSize: 12
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
