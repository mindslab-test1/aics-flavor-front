import 'dart:ui';

import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:heystars_app/screens/component/cards/myinfo/notice_card.dart';

class NotciePage extends StatefulWidget {
  @override
  _NotciePageState createState() => _NotciePageState();
}

class _NotciePageState extends State<NotciePage> {



  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    final screenHeight = MediaQuery
        .of(context)
        .size
        .height;

    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          centerTitle: true,
          title: Text('Notice',
            style: TextStyle(
                fontFamily: 'NotoSansCJKkr',
                fontSize: 16,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(51, 51, 51, 1)
            ),),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              padding: EdgeInsets.only(left: screenWidth * 0.067),
              child: Image.asset('assets/images/intro/arrow_left.png',
                width: 11.5,),
            ),
          )
      ),
      body: Container(
        color: Colors.white,
        child: Container(
          child: Column(
            children: [
              NoticeCard(title: 'Service Open', date: '2020.06.20 10:29', content: 'Welcome!\nFlavor Service is open.\nLorem lpsum is simply dummy text of the printing and typesetting industry.', upload: true,),
              NoticeCard(title: '[서비스 소식] AAC 256kbps 이상 음질 서비스 안내 두줄 일때는 이렇게 보여주면 됩니다.', date: '2020.06.19 23:59', content: 'content', upload: false,)
            ],
          ),
        )
      ),
    );
  }
}
