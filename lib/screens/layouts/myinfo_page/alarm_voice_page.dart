import 'package:flutter/material.dart';
import 'package:heystars_app/screens/component/cards/myinfo/voice_btn_card.dart';

class VoicePage extends StatefulWidget {
  @override
  _VoicePageState createState() => _VoicePageState();
}

class _VoicePageState extends State<VoicePage> {

  bool checked = true;

  List<String> voiceNames = ['EunJung', 'Whoareyou', 'eunjung'];
  List<String> voiceOnImgs = ['assets/images/chatbot/profile_eunjung.png', 'assets/images/chatbot/profile_eunjung.png', 'assets/images/chatbot/profile_eunjung.png'];
  List<String> voiceOffImgs = ['assets/images/chatbot/profile_eunjung.png', 'assets/images/chatbot/profile_eunjung.png', 'assets/images/chatbot/profile_eunjung.png'];



  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          centerTitle: true,
          title: Text('Voice',
            style: TextStyle(
                fontFamily: 'NotoSansCJKkr',
                fontSize: 16,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(51, 51, 51, 1)
            ),),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              padding: EdgeInsets.only(left: screenWidth * 0.067),
              child: Image.asset('assets/images/intro/arrow_left.png',
                width: 11.5,),
            ),
          )
      ),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.only(left: screenWidth * 0.067, right: screenWidth * 0.067, top: 14, bottom: 14),
        child: VoiceBtnCard(names: voiceNames, onImgs: voiceOnImgs, offImgs: voiceOffImgs, category: 'Voice',)
      ),
    );
  }
}
