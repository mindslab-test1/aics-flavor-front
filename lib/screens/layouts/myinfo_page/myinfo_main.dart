import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:heystars_app/providers/myinfo_provider.dart';
import 'package:heystars_app/providers/user_provider.dart';
import 'package:heystars_app/screens/component/cards/myinfo/info_card.dart';
import 'package:heystars_app/screens/component/cards/myinfo/info_menu_card.dart';
import 'package:heystars_app/screens/component/cards/myinfo/info_move_text.dart';
import 'package:heystars_app/screens/layouts/myinfo_page/term_conditions_page.dart';
import 'package:heystars_app/services/login_auth/firebase_auth_utils.dart';
import 'package:heystars_app/services/repositories/myinfo_shared_preferences.dart';
import 'package:provider/provider.dart';

class MyInfoMain extends StatefulWidget {
  AuthFunc auth;
  VoidCallback onSignOut;

  MyInfoMain({
    Key key,
    this.auth,
    this.onSignOut,
  }):super(key:key);


  @override
  _MyInfoMainState createState() => _MyInfoMainState();
}

class _MyInfoMainState extends State<MyInfoMain> {
  MyInfoProvider myInfoProvider;
  SharedPrefs sharedPrefs = SharedPrefs();
  String policy;

  Future<void>_getPrefs() async{
    await sharedPrefs.init();
    return sharedPrefs;
  }

  // txt 파일을 읽어오는 곳곳
 Future<String> readPolicy() async {
    policy = await rootBundle.loadString('assets/Privacy Policy_0921.docx.txt');
  }



  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    Size size = MediaQuery.of(context).size;

    UserProvider userProvider = Provider.of<UserProvider>(context, listen: false);
    myInfoProvider = Provider.of<MyInfoProvider>(context);
    readPolicy();

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          width: screenWidth,
          padding: EdgeInsets.only(top: 16, bottom: 16, left: 24, right: 24),
          child: Text('My Info',
          style: TextStyle(
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.w600,
            fontSize: 18
          ),
          textAlign: TextAlign.start,),
        ),
        Container(
          padding: EdgeInsets.only(left: 24),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                  width: screenWidth * 0.178,
                  height: screenHeight * 0.087,
                  child: SvgPicture.asset('assets/images/myinfo/picture_add-64px.svg',),
                ),
              SizedBox(
                width: screenWidth * 0.033,
              ),
              Text("I'm ${userProvider.nickname}",
              style: TextStyle(
                fontSize: 16,
                fontFamily: 'NotoSansCJKkr',
                fontWeight: FontWeight.w500
              ),)
            ],
          ),
        ),
        FutureBuilder(
          future: _getPrefs(),
          builder: (context, AsyncSnapshot snapshot){
            if (snapshot.hasData){
              return Container(
                padding: EdgeInsets.only(left: screenWidth * 0.067),
                child: Row(
                  children: [
                    /*InfoCard(cardName: 'Alarm', imgPath: myInfoProvider.voiceImg ==null? sharedPrefs.voiceImg:myInfoProvider.voiceImg,
                    imgShape: true, imgName: myInfoProvider.voiceName ==null? sharedPrefs.voiceName:myInfoProvider.voiceName,
                      cardInfo: myInfoProvider.alarmDate ==null? sharedPrefs.alarmDate: myInfoProvider.alarmDate, imgWidth: screenWidth * 0.067,
                    imgHeight: screenHeight * 0.032, ),
                    SizedBox(
                      width: screenWidth * 0.033,
                    ),*/
                    InfoCard(cardName: 'Language', imgPath: myInfoProvider.languageImg ==null? sharedPrefs.languageImg:myInfoProvider.languageImg,
                    imgShape: false, imgName: myInfoProvider.languageNation ==null? sharedPrefs.languageNation: myInfoProvider.languageNation,
                    cardInfo: myInfoProvider.languageName ==null? sharedPrefs.languageName: myInfoProvider.languageName, imgWidth: screenWidth * 0.083,
                    imgHeight: screenHeight * 0.032,),
                  ],
                ),
              );
            }
            return Text(snapshot.error.toString());
          },
        ),
        Container(
          margin: EdgeInsets.only(top: screenHeight * 0.021),
          width: screenWidth * 0.867,
          child: Column(
            children: [
              InfoMenuCard(menuName: 'Notice'),
              InfoMenuCard(menuName: 'CS Center'),
              InfoMenuCard(menuName: 'Payment')
            ],
          )
        ),
        GestureDetector(
          child: InfoMoveText(moveName: 'Privacy policy',),
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder:(BuildContext context) => TermConditionsPage(policy: policy,)));
          },
        ),
        GestureDetector(
          onTap: (){
           _signOut();
          },
          child: InfoMoveText(moveName: 'Sign Out',),
        ),
        // FlatButton(
        //   child: Text('popup'),
        //   onPressed: (){
        //   _studyPopUp(size);
        // }),
      ],
    );
  }


  void _signOut() async{
    try{
      await widget.auth.signOut();
      widget.onSignOut();
    }catch(e){
      print(e);
    }
  }

  void _studyPopUp(Size size){
    showDialog(context: context,
        builder: (BuildContext context){
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)
              ),
              contentPadding: EdgeInsets.all(0.0),
              content: Container(
                width: size.width * 0.867,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      height: 284,
                        child: Stack(
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20),
                              ),
                              child: Container(
                                color: Colors.blue,
                              ),
                            ),
                            Positioned(
                              top: 16, right: 16,
                              child: InkWell(
                                child: SvgPicture.asset('assets/images/myinfo/close-24px.svg'),
                                onTap: (){
                                  Navigator.pop(context);
                                },
                              )
                            )
                          ],
                        )
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 16, bottom: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text('Monica, 학습 할 시간이예요.',
                            style: TextStyle(
                              fontSize: 14,
                              fontFamily: 'NotoSansCJKkr',
                              fontWeight: FontWeight.w700,
                              color: Color.fromRGBO(51, 51, 51, 1),
                            ),),
                            Text('Monica, It is time to study!',
                              style: TextStyle(
                                fontSize: 12,
                                fontFamily: 'NotoSansCJKkr',
                                fontWeight: FontWeight.w400,
                                color: Color.fromRGBO(51, 51, 51, 1),
                              ),),
                            SizedBox(
                              height: 16,
                            ),
                            Container(
                                width: size.width * 0.867 * 0.487,
                                height: 51,
                                child: RaisedButton(
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(26)),
                                  color: Color.fromRGBO(39, 215, 148, 1),
                                  child:Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        '학습하기',
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontFamily: 'NotoSansCJKkr',
                                            fontWeight: FontWeight.w700,
                                            color: Colors.white),
                                      ),
                                      Text(
                                        'Study',
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontFamily: 'NotoSansCJKkr',
                                            fontWeight: FontWeight.w500,
                                            color: Colors.white),
                                      ),
                                    ],
                                  ),
                                  onPressed: () {
                                    print('학습하기');
                                  },
                                )),
                          ],
                        ),
                      ),
                  ],
                ),
              )
          );
        }
    );
  }
}
