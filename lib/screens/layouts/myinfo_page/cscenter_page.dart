import 'package:flutter/material.dart';

class CsCenterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
        title: Text('CS Center',
          style: TextStyle(
              fontFamily: 'NotoSansCJKkr',
              fontSize: 16,
              fontWeight: FontWeight.w500,
            color: Color.fromRGBO(51, 51, 51, 1)
          ),),
        leading: GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: Container(
            padding: EdgeInsets.only(left: screenWidth * 0.084),
            child: Image.asset('assets/images/intro/arrow_left.png',
            width: 11.5,),
          ),
        )
      ),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.only(left: screenWidth * 0.067, top: screenHeight * 0.329),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Customer \nService Center',
                style: TextStyle(
                    fontSize: 32,
                    fontFamily: 'SpoqaHanSans',
                    fontWeight: FontWeight.w700,
                    color: Color.fromRGBO(51, 51, 51, 1)
                ),),
              Text('Contact us at help@hey-stars.com',
                style: TextStyle(
                    fontSize: 14,
                    fontFamily: 'SpoqaHanSans',
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(51, 51, 51, 1)
                ),)
            ],
          ),
        ),
      ),
    );
  }
}
