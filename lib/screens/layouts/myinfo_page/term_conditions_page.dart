import 'dart:ui';
import 'package:flutter/material.dart';

class TermConditionsPage extends StatelessWidget {
  final String policy;

  const TermConditionsPage({Key key, this.policy}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;


    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          centerTitle: true,
          title: Text(
            'Privacy policy',
            style: TextStyle(
                fontFamily: 'NotoSansCJKkr',
                fontSize: 16,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(51, 51, 51, 1)),
          ),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              padding: EdgeInsets.only(left: screenWidth * 0.067),
              child: Image.asset(
                'assets/images/intro/arrow_left.png',
                width: 11.5,
              ),
            ),
          )),
      body: Container(
          color: Colors.white,
          width: screenWidth,
          height: screenHeight,
          child: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(left: screenWidth * 0.067, right: screenWidth * 0.067),
              padding: EdgeInsets.all(12),
              child: Text(policy,
              style: TextStyle(
                fontFamily: 'NotoSansCJKkr',
                fontSize: 12,
                fontWeight: FontWeight.w400
              ),),
            ),
          )
      ),
    );
  }
}
