import 'package:flutter/material.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';
import 'package:heystars_app/providers/myinfo_provider.dart';
import 'package:heystars_app/screens/component/button/alarm_day_btn.dart';
import 'package:heystars_app/screens/component/cards/myinfo/info_menu_card.dart';
import 'package:heystars_app/services/repositories/myinfo_shared_preferences.dart';

import 'package:provider/provider.dart';

class AlarmPage extends StatefulWidget {
  @override
  _AlarmPageState createState() => _AlarmPageState();
}

class _AlarmPageState extends State<AlarmPage> {
  DateTime alarmTime;
  SharedPrefs sharedPrefs = SharedPrefs();


  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    MyInfoProvider myInfoProvider = Provider.of<MyInfoProvider>(context);
    String minute;


    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          centerTitle: true,
          title: Text(
            'Alarm',
            style: TextStyle(
                fontFamily: 'NotoSansCJKkr',
                fontSize: 16,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(51, 51, 51, 1)),
          ),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              padding: EdgeInsets.only(left: screenWidth * 0.067),
              child: Image.asset(
                'assets/images/intro/arrow_left.png',
                width: 11.5,
              ),
            ),
          )),
      body: Container(
          width: screenWidth,
          color: Colors.white,
          child: Container(
            margin: EdgeInsets.only(
                left: screenWidth * 0.067, right: screenWidth * 0.067),
            padding: EdgeInsets.only(
                top: screenHeight * 0.047, bottom: screenHeight * 0.047),
            child: Column(
              children: [
                TimePickerSpinner(
                  time: myInfoProvider.alarmDateOrigin == null? DateTime.parse(sharedPrefs.alarmDateOrigin):myInfoProvider.alarmDateOrigin,
                  is24HourMode: false,
                  spacing: screenWidth * 0.115,
                  itemHeight: 38,
                  isForce2Digits: true,
                  onTimeChange: (time) {
                    alarmTime = time;
                  },
                  normalTextStyle: TextStyle(
                      fontSize: 18,
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w800,
                      color: Color.fromRGBO(221, 221, 221, 1),
                      letterSpacing: -0.45),
                  highlightedTextStyle: TextStyle(
                      fontSize: 24,
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w800,
                      color: Color.fromRGBO(51, 51, 51, 1),
                      letterSpacing: -0.6),
                ),
                Divider(
                  thickness: 1,
                  color: Color.fromRGBO(242, 242, 242, 1),
                ),
                Container(
                  width: screenWidth,
                  padding: EdgeInsets.only(
                      top: screenHeight * 0.032, bottom: screenHeight * 0.032),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Day',
                          style: TextStyle(
                            fontSize: 14,
                            fontFamily: 'NotoSansCJKkr',
                            fontWeight: FontWeight.w500,
                            color: Color.fromRGBO(51, 51, 51, 1),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: screenHeight * 0.016,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              AlarmDayBtn(
                                short: 'S', idx: 0, full: 'SUN',
                              ),
                              AlarmDayBtn(
                                short: 'M',idx: 1, full: 'MON',
                              ),
                              AlarmDayBtn(
                                short: 'T',idx: 2, full: 'TUE',
                              ),
                              AlarmDayBtn(
                                short: 'W',idx: 3, full: 'WEN',
                              ),
                              AlarmDayBtn(
                                short: 'T',idx: 4, full: 'THU',
                              ),
                              AlarmDayBtn(
                                short: 'F',idx: 5, full: 'FRI',
                              ),
                              AlarmDayBtn(
                                short: 'S',idx: 6, full: 'SAT',
                              ),
                            ],
                          ),
                        ),
                      ]),
                ),
                Divider(
                  thickness: 1,
                  color: Color.fromRGBO(242, 242, 242, 1),
                ),
                SizedBox(
                  height: screenHeight * 0.016,
                ),
                InfoMenuCard(
                  menuName: 'Voice',
                  content: myInfoProvider.voiceName ==null? sharedPrefs.voiceName:myInfoProvider.voiceName,
                ),
                Expanded(child: Container()),
                Container(
                    width: screenWidth,
                    height: screenHeight * 0.063,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      color: Color.fromRGBO(39, 215, 148, 1),
                      child: Center(
                        child: Text(
                          'Save',
                          style: TextStyle(
                              fontSize: 16,
                              fontFamily: 'NotoSansCJKkr',
                              fontWeight: FontWeight.w700,
                              color: Colors.white),
                        ),
                      ),
                      onPressed: () {
                        if(alarmTime.minute.toString() != null && alarmTime.minute.toString().length ==1){
                          minute = '0' + alarmTime.minute.toString();
                        }else{
                          minute = alarmTime.minute.toString();
                        }
                        setState(() {
                          myInfoProvider.setAlarmDateOrigin(alarmTime);
                          myInfoProvider.setAlarmDate('${alarmTime.hour}' +
                              ':' +
                             minute);
                          sharedPrefs.setAlarmDateOrigin = alarmTime.toIso8601String();
                          sharedPrefs.setAlarmDate = '${alarmTime.hour}' +
                              ':' +
                              minute;
                          Navigator.pop(context);
                        });
                      },
                    )),
              ],
            ),
          )),
    );
  }
}
