import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:heystars_app/screens/component/cards/myinfo/payment_card.dart';

class PaymentPage extends StatefulWidget {
  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          centerTitle: true,
          title: Text('Payment',
            style: TextStyle(
                fontFamily: 'NotoSansCJKkr',
                fontSize: 16,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(51, 51, 51, 1)
            ),),
          leading: GestureDetector(
            onTap: (){
              Navigator.pop(context);
            },
            child: Container(
              padding: EdgeInsets.only(left: screenWidth * 0.067),
              child: Image.asset('assets/images/intro/arrow_left.png',
                width: 11.5,),
            ),
          )
      ),
      body: Container(
        color: Colors.white,
        width: screenWidth,
        height: screenHeight,
        child: Column(
          children: [
            PaymentCard(),
            SizedBox(
              height: 8,
            ),
            PaymentCard()
          ],
        )
      ),
    );
  }
}
