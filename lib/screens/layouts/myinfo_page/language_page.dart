import 'package:flutter/material.dart';
import 'package:heystars_app/screens/component/cards/myinfo/language_btn_card.dart';
import 'package:heystars_app/services/repositories/myinfo_shared_preferences.dart';

class LanguagePage extends StatefulWidget {

  @override
  _LanguagePageState createState() => _LanguagePageState();
}

class _LanguagePageState extends State<LanguagePage> {

  bool checked = true;

  List<String> Languagenames = ['English', 'Indonesian', 'Vietnamese'];

  List<String> LanguageonImgs = ['assets/images/myinfo/free-icon-united-states-of-america-940207.svg',
    'assets/images/myinfo/free-icon-indonesia.svg',
    'assets/images/myinfo/free-icon-vietnam-940212.svg',
  ];

  List<String>LanguageoffImgs = ['assets/images/myinfo/free-icon-off-united-states-of-america-940207.svg',
    'assets/images/myinfo/free-icon-off-indonesia-940247.svg',
    'assets/images/myinfo/free-icon-off-vietnam-940212.svg'
  ];

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          centerTitle: true,
          title: Text('Language',
            style: TextStyle(
                fontFamily: 'NotoSansCJKkr',
                fontSize: 16,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(51, 51, 51, 1)
            ),),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              padding: EdgeInsets.only(left: screenWidth * 0.067),
              child: Image.asset('assets/images/intro/arrow_left.png',
                width: 11.5,),
            ),
          )
      ),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.only(left: screenWidth * 0.1, right: screenWidth * 0.1, top: 14, bottom: 14),
        child: LanguageBtnCard(names: Languagenames, onImgs: LanguageonImgs, offImgs: LanguageoffImgs, category: 'Lang',)
      ),
    );
  }
}
