import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:heystars_app/screens/component/cards/signIn_signUp/sign_In_up_textField.dart';
import 'package:heystars_app/services/login_auth/firebase_auth_utils.dart';

class SignUpPage extends StatefulWidget {

  final AuthFunc auth;
  final Function goSignIn;
  final VoidCallback onSignedIn;

  SignUpPage({this.auth, this.onSignedIn, this.goSignIn});

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {

  final _formKey = new GlobalKey<FormState>();

  String _email, _password, _confirmPassword, _erroMessage;
  String policy;
  bool _isConfirmPolicy = false;
  bool _isLoading;

  bool _validateAndSave(){
    final form = _formKey.currentState;
    if(form.validate()){
      form.save();
      return true;
    }

    return false;
  }

  void _validateAndSubmit() async{
    setState(() {
      _isLoading = true;
      _erroMessage = "";
    });

    if(_validateAndSave()) {
      String userId = "";
      if (_password != _confirmPassword) {
        setState(() {
          _erroMessage = 'password and confirm password are not matching';
        });
      } else {
        try {
          userId = await widget.auth.signUp(_email, _password);
          widget.goSignIn();
          _showVerifyEmailSentDialog();

          setState(() {
            _isLoading = false;
          });
        } catch (e) {
          List<String> str = e.toString().split(' ');

          setState(() {
            _isLoading = false;
            for (int i = 0; i < str.length; i++)
              if (i != 0) _erroMessage = _erroMessage + ' ' + str[i];
          });
        }
      }
    }
  }

  void _showVerifyEmailSentDialog() {
    widget.auth.sendEmailVerification();

    showDialog(context: context,
        builder: (BuildContext context){
          return AlertDialog(
            title: Text('Thank you'),
            content: Text('Link verify has been sent to your email'),
            actions: [
              new FlatButton(
                  onPressed: (){
                    Navigator.of(context).pop();
                  },
                  child: Text('OK'))
            ],
          );
        }
    );
  }

  @override
  void initState() {
    super.initState();

    _erroMessage = "";
    _isLoading = false;
    getPolicy();
  }

  getPolicy() async{
    String getPolicy =  await rootBundle.loadString('assets/Privacy Policy_0921.docx.txt');
    setState(() {
      policy = getPolicy;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      //child: signUpForm(size),
      child: _isConfirmPolicy?signUpForm(size):confirmPolicy(size)
    );
  }



  signUpForm(Size size){
    return Container(
      padding: EdgeInsets.all(16),
      child: Form(
        key: _formKey,
        child: Padding(
          padding: EdgeInsets.only(left: 12, right: 12),
          child: ListView(
            shrinkWrap: true,
            children: [
              _signUpTitle(),
              _emailInput(),
              _passwordInput(),
              _passwordConfirmInput(),
              _signupBtn(size),
              _showErrorMessage(),
              goSignInBtn()
            ],
          ),
        ),
      ),
    );
  }

  _signUpTitle(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Let’s Get Started',
          style: TextStyle(
            fontSize: 24,
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.w800,
            color: Color(0xff333333)
          ),
        ),
        SizedBox(height: 4,),
        Text(
          'Create an account to learn Korean!',
          style: TextStyle(
            fontSize: 12,
            fontFamily: 'NotoSansCJKkr',
            fontWeight: FontWeight.w400,
            color: Color(0xff999999)
          ),
        )
      ],
    );
  }

  _emailInput(){
    return Padding(
      padding: EdgeInsets.only(top: 24),
      child: SignInUpTextField(
          'Email Address',
          TextInputType.emailAddress,
          false,
              (var value){
            return value.isEmpty ? 'email can not be empty' : null;
          },
              (String value){
            _email = value.trim();
          }
      ),
    );
  }
  _passwordInput(){
    return Padding(
      padding: EdgeInsets.only(top: 16),
      child: SignInUpTextField(
          'Password',
          null,
          true,
              (var value){
            return value.isEmpty ? 'password can not be empty' : null;
          },
              (String value){
            _password = value.trim();
          }
      ),
    );
  }

  _passwordConfirmInput(){
    return Padding(
      padding: EdgeInsets.only(top: 16),
      child: SignInUpTextField(
          'Confirm Password',
          null,
          true,
              (var value){
            return value.isEmpty ? 'confirm password can not be empty' : null;
          },
              (String value){
            _confirmPassword = value.trim();
          }
      ),
    );
  }

  _signupBtn(Size size){
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 24, 0, 0),
      child: SizedBox(
        height: size.height*0.07,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          color: Color(0xff27d794),
          onPressed: _validateAndSubmit,
          elevation: 0,
          child: Text(
            'Create',
            style: TextStyle(
              fontFamily: 'NotoSansCJKkr',
              fontSize: 16,
              fontWeight: FontWeight.w700,
              color: Colors.white
            ),
          ),
        ),
      ),
    );
  }

  goSignInBtn(){
    return Padding(
      padding: EdgeInsets.only(top: 24),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Already have an account?',
            style: TextStyle(
                fontSize: 13,
                color: Color(0xff333333),
                fontFamily: 'NotoSansCJKkr',
                fontWeight: FontWeight.w400
            ),
          ),
          SizedBox(width: 13,),
          GestureDetector(
            onTap: (){
              widget.goSignIn();
            },
            child: Text(
              'Sign in',
              style: TextStyle(
                  fontSize: 12,
                  color: Color(0xff27d794),
                  fontFamily: 'NotoSansCJKkr',
                  fontWeight: FontWeight.w500,
                  decoration: TextDecoration.underline
              ),
            ),
          )
        ],
      ),
    );
  }

  confirmPolicy(Size size){
    return Container(
      width: size.width,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(24, 36, 24, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Privacy Policy',
              style: TextStyle(
                  fontSize: 24,
                  color: Colors.black,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w800
              ),
            ),
            SizedBox(height: 24,),
            Container(
              height: 324,
              width: double.infinity,
              color: (policy==null)?Colors.white:Color(0xfff5f5f5),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: SingleChildScrollView(
                    child: Text(
                        (policy==null)?'':policy,
                      style: TextStyle(
                        fontFamily: 'NotoSansCJKkr',
                        fontWeight: FontWeight.w500
                      ),
                    )
                ),
              ),
            ),
            SizedBox(height: 24,),
            SizedBox(
              width: double.infinity,
              child: RaisedButton(
                color: Color(0xff27d794),
                elevation: 0,
                onPressed: (){
                  setState(() {
                    _isConfirmPolicy = true;
                  });
                },
                child: Padding(
                    padding: EdgeInsets.only(top: 15, bottom: 15),
                    child: Text(
                      'OK',
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'NotoSansCJKkr'
                      ),
                    )
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)
                ),
              ),
            ),
            goSignInBtn()
          ],
        ),
      ),
    );
  }

  Widget _showErrorMessage(){
    if(_erroMessage != null && _erroMessage.length > 0){
      return Text(_erroMessage);
    }
    else{
      return new Container(height: 0, width: 0,);
    }
  }
}
