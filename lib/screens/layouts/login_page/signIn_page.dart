import 'package:flutter/material.dart';
import 'package:heystars_app/screens/component/cards/signIn_signUp/sign_In_up_textField.dart';
import 'package:heystars_app/screens/layouts/login_page/signup_page.dart';
import 'package:heystars_app/services/common/popup.dart';
import 'package:heystars_app/services/login_auth/firebase_auth_utils.dart';

class SignInPage extends StatefulWidget {

  AuthFunc auth;
  Function goSignUp;
  VoidCallback onSignedIn;

  SignInPage({this.auth, this.onSignedIn, this.goSignUp});

  @override
  _SignInSignUpPageState createState() => _SignInSignUpPageState();
}

class _SignInSignUpPageState extends State<SignInPage> {

  final _formKey = new GlobalKey<FormState>();

  String _email, _password, _errorMessage;

  bool _isIos, _isLoading;

  bool _validateAndSave(){
    final form = _formKey.currentState;
    if(form.validate()){
      form.save();
      return true;
    }

    return false;
  }

  void _validateAndSubmit() async{
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });

    if(_validateAndSave()){
      String userId = "";
      try{
        userId = await widget.auth.signIn(_email, _password);
        if(userId != null) widget.onSignedIn();

        setState(() {
          _isLoading = false;
        });
      }catch(e) {
        setState(() {
          _isLoading=false;
          if(_isIos)
            _errorMessage = 'ios Error';
          else{
            List<String> str = e.toString().split(' ');
            for(int i =0;i<str.length;i++){
              if(i!=0) _errorMessage = _errorMessage+' '+ str[i];
            }
          }
        });
      }
    }
  }

  void googleSignIn() async{
    String userId = "";
    try{
      userId = await widget.auth.googleSignInFunc();
      if(userId != null) widget.onSignedIn();

      setState(() {
        _isLoading = false;
      });
    }catch(e) {
      _isLoading = false;
      _errorMessage = e.toString();
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _errorMessage = "";
    _isLoading = false;
  }

  @override
  Widget build(BuildContext context) {
    _isIos = Theme.of(context).platform == TargetPlatform.iOS;
    Size size = MediaQuery.of(context).size;

    return Container(
      child: Stack(
        children: [
          showBody(size),
          //showCircularProgress(),
        ],
      ),
    );
  }

  showCircularProgress() {
    if(_isLoading)
      return Center(child: CircularProgressIndicator(),);
    return Container(height: 0, width: 0,);
  }

  showBody(Size size) {
    return new Container(
      padding: EdgeInsets.all(24),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: [
            _showText(),
            SizedBox(height: size.height*0.033,),
            _showEmailInput(),
            SizedBox(height: 16,),
            _showPasswordInput(),
            SizedBox(height: 8,),
         //   _ForgotPassword(),
            _showButton(),
            SizedBox(height: size.height*0.038,),
            _googleSinginBtn(size),
            _showErrorMessage(),
            _signUp(size)
          ],
        ),
      ),
    );
  }

  Widget _showErrorMessage(){
    if(_errorMessage != null && _errorMessage.length > 0){
      return Text(_errorMessage);
    }
    else{
      return new Container(height: 0, width: 0,);
    }
  }

  Widget _ForgotPassword(){
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 9, 0, 9),
      child: Text(
        'Forgot password?',
        style: TextStyle(
          color: Color(0xff27d794),
          fontSize: 12,
          fontFamily: 'NotoSansCJKkr',
          fontWeight: FontWeight.w500
        ),
      ),
    );
  }

  Widget _showButton() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 25, 0, 0),
      child: SizedBox(
        child: RaisedButton(
          color: Color(0xff27d794),
          onPressed: _validateAndSubmit,
          elevation: 0,
          child: Padding(
            padding: EdgeInsets.only(top: 15, bottom: 15),
              child: Text(
                'Sign In',
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontFamily: 'NotoSansCJKkr'
                ),
              )
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8)
          ),
        ),
      ),
    );
  }

  _showEmailInput(){
    return SignInUpTextField(
        'Email',
        TextInputType.emailAddress,
        false,
            (var value){
          return value.isEmpty ? 'email can not be empty' : null;
        },
            (String value){
          _email = value.trim();
        }
    );
  }

  _showPasswordInput(){
    return SignInUpTextField(
        'Password',
        null,
        true,
            (var value){
          return value.isEmpty ? 'password can not be empty' : null;
        },
            (String value){
          _password = value.trim();
        }
    );
  }

  _showText(){
    return Hero(
      tag: 'here',
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Sign in',
            style: TextStyle(
              color: Color(0xff333333),
              fontSize: 24,
              fontFamily: 'Montserrat',
              fontWeight: FontWeight.w800
            ),
          ),
          SizedBox(height: 4,),
          Text(
            'Start Flavor with Heystars!',
            style: TextStyle(
              color: Color(0xff999999),
              fontSize: 12,
              fontFamily: 'NotoSansCJKkr',
              fontWeight:FontWeight.w400
            ),
          )
        ],
      ),
    );
  }

  _signUp(Size size){
    return Padding(
      padding: EdgeInsets.only(top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Dont have an account?',
            style: TextStyle(
              fontSize: 13,
              color: Color(0xff333333),
              fontFamily: 'NotoSansCJKkr',
              fontWeight: FontWeight.w400
            ),
          ),
          SizedBox(width: 11,),
          GestureDetector(
            child: Text(
              'Sign Up',
              style: TextStyle(
                color: Color(0xff27d794),
                fontSize: 12,
                fontFamily: 'NotoSansCJKk',
                fontWeight: FontWeight.w500,
                decoration: TextDecoration.underline
              ),
            ),
            onTap: (){
              Popup().privacyPolicyPopup(context, size, widget.goSignUp);
            },
          )
        ],
      ),
    );
  }

  _googleSinginBtn(Size size) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 48,
          height: 48,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Color(0xfff2f2f2),
          ),
          child: InkWell(
            onTap: (){
              googleSignIn();
            },
           child: Padding(
             padding: EdgeInsets.all(10),
             child: Image.asset(
               'assets/images/intro/signIn_with_google2.png',
               fit: BoxFit.fitHeight,
             ),
           ),
          ),
        ),
      ],
    );
  }
}

