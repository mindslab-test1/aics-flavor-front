import 'package:flutter/material.dart';

SignInUpTextField(String text, TextInputType textInputType, bool isObscureText,
    Function validator, Function onSaved){
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        text,
        style: TextStyle(
            fontSize: 12,
            fontFamily: 'NotoSansCJKkr',
            fontWeight: FontWeight.w500,
            color: Color(0xffbbbbbb)
        ),
      ),
      SizedBox(height: 4,),
      TextFormField(
        keyboardType: (textInputType==null)?null:textInputType,
        maxLines: 1,
        obscureText: isObscureText,
        autofocus: false,
        decoration: InputDecoration(
          hintText: text,
          hintStyle: TextStyle(
            fontSize: 14,
            color: Color(0xffcccccc),
            fontFamily:'NotoSansCJKkr',
            fontWeight: FontWeight.w400
          ),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xff27d794)),
              borderRadius: BorderRadius.circular(8),
          ),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xff27d794)),
              borderRadius: BorderRadius.circular(8)
          ),
        ),
        validator: (value) => validator(value),
        onSaved: (value) => onSaved(value),
      ),
    ],
  );
}