import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:heystars_app/providers/course_provider.dart';
import 'package:heystars_app/screens/component/cards/course/course_card.dart';
import 'package:heystars_app/services/json_parser/course/chapters_parser.dart';
import 'package:provider/provider.dart';


class HomeCourse extends StatefulWidget {

  Function movePickPage;
  HomeCourse({
    this.movePickPage
  });

  @override
  _HomeCourseState createState() => _HomeCourseState();
}

class _HomeCourseState extends State<HomeCourse> {

  CourseProvider courseProvider;
  List<Widget> cardList;
  int cardCount;
  bool isLoading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    cardCount = 0;
    courseProvider = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    createCards(size);

    return (isLoading)?Container():SliverPadding(
      padding: EdgeInsets.only(left: 24, right: 24, bottom: 50),
      sliver: SliverGrid(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 16,
          mainAxisSpacing: 16
        ),
        delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index){
              return cardList[index];
            },
            //childCount: courseProvider.chaptersParser.chapters.length
          childCount: cardCount
        ),
      ),
    );
  }

  createCards(Size size){
    List<Widget> cards = List();
    cardCount = 0;

    courseProvider.chaptersParser.chapters.forEach((chapter) {
      if(cardCount<4) {
        Widget card = GestureDetector(
          onTap: (){
            widget.movePickPage('Course');
          },
          child: Container(
            color: Colors.transparent,
            child: CourseCard(
              color: courseProvider.chaptersParser.chapters.indexOf(chapter) % 4,
              img: chapter.imgUrl,
              name: chapter.titleKor,
              eng: chapter.titleEng,
              size: size,
            ),
          ),
        );

        cards.add(card);
        cardCount+=1;

      }
    });
    setState(() {
      cardList = cards;
      isLoading = false;
    });
  }

}
