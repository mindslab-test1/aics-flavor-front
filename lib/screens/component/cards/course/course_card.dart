import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:heystars_app/services/common/common.dart';

class CourseCard extends StatelessWidget {

  final int color;
  final String img;
  final String name;
  final String eng;
  final Size size;

  CourseCard({
    this.color,
    this.img,
    this.name,
    this.eng,
    this.size
  });


  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: Common().getCardGradientColor(color)
        ),
      ),
      child: Container(
        child: Padding(
          padding: EdgeInsets.fromLTRB(18, 24, 18, 24),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  width: size.width*0.2,
                  height: size.width*0.2,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white.withOpacity(0.2),
                  ),
                  child: Center(
                    child: SvgPicture.network(img),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: Text(
                  'Level1',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 10,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      name,
                      style: TextStyle(
                          fontSize: 14,
                          color: Colors.white,
                          fontFamily: 'NotoSansCJKkr',
                          fontWeight: FontWeight.w700
                      ),
                    ),
                    Text(
                      eng,
                      style: TextStyle(
                          fontSize: 10,
                          color: Colors.white,
                          fontFamily: 'NotoSansCJKkr',
                          fontWeight: FontWeight.w300
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  
}
