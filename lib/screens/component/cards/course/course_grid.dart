import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:heystars_app/providers/course_provider.dart';
import 'package:heystars_app/screens/component/cards/course/course_card.dart';
import 'package:provider/provider.dart';

class SliverGridCard extends StatefulWidget {
  final Size size;

  SliverGridCard({
    this.size
  });

  @override
  _SliverGridCardState createState() => _SliverGridCardState();
}

class _SliverGridCardState extends State<SliverGridCard> {

  CourseProvider courseProvider;

  @override
  void initState() {
    super.initState();
    courseProvider = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return SliverGrid(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 16,
          mainAxisSpacing: 16
      ),
      delegate: SliverChildListDelegate(
          createCard(widget.size)
      ),
    );
  }

  List<Widget> createCard(Size size){
    List<Widget> cards = List();

    for(int i=0; i<courseProvider.chaptersParser.chapters.length;i++){
      Widget card = GestureDetector(
        onTap: (){
          setState(() {
            courseProvider.setSelectedChapter(i);
          });
        },
        child: Stack(
          children: [
            CourseCard(
              color: i%4,
              img: courseProvider.chaptersParser.chapters[i].imgUrl,
              name: courseProvider.chaptersParser.chapters[i].titleKor,
              eng: courseProvider.chaptersParser.chapters[i].titleEng,
              size: size,
            ),
            SizedBox.expand(
              child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: (courseProvider.selectedChapter==i)
                        ?Colors.black.withOpacity(0.7)
                        :Colors.transparent,
                  ),
                  child: Center(
                    child: Container(
                      width: 60,
                      height: 60,
                      child: SvgPicture.asset(
                        'assets/images/course/check-48px.svg',
                        color: (courseProvider.selectedChapter==i)
                            ?Colors.white
                            :Colors.transparent,
                      ),
                    ),
                  )
              ),
            )
          ],
        ),
      );

      cards.add(card);
    }

    return cards;
  }
}
