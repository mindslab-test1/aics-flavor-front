import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:heystars_app/providers/course_provider.dart';
import 'package:heystars_app/screens/layouts/tutor_page/tutor_detail.dart';
import 'package:provider/provider.dart';

class TutorSlider extends StatefulWidget {

  final bool isHomePage;
  Function movePickPage;

  TutorSlider({this.isHomePage, this.movePickPage});

  @override
  _TutorSliderState createState() => _TutorSliderState();
}

class _TutorSliderState extends State<TutorSlider> {
  CourseProvider courseProvider;
  int _current = 0;

  @override
  void initState() {
    super.initState();
    courseProvider = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.only(bottom: 20),
      child: Column(
        children: [
          CarouselSlider(
            options: CarouselOptions(
              aspectRatio: 0.9,
              enlargeCenterPage: true,
              scrollDirection: Axis.horizontal,
              autoPlay: true,
              onPageChanged: (index, reason){
                setState(() {
                  _current = index;
                });
              }
            ),
            items: tutorSliders(size),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: courseProvider.tutorParser.tutors.map((tutor) {
              int index = courseProvider.tutorParser.tutors.indexOf(tutor);
              return Container(
                width: 4.0,
                height: 4.0,
                margin: EdgeInsets.symmetric(
                  vertical: 10,
                  horizontal: 2
                ),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _current == index
                    ? Color(0xFF27d794)
                    : Color(0xFFd0d0d0)
                ),
              );
            }).toList(),
          )
        ],
      ),
    );
  }

  List<Widget> tutorSliders (Size size) {
    List<Widget> tutorSliders = courseProvider.tutorParser.tutors.map((tutor) {
      int index = courseProvider.tutorParser.tutors.indexOf(tutor);

      return Container(
        child: Container(
          margin: EdgeInsets.all(5),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            child: Stack(
              children: [
                Opacity(
                  opacity: (index == _current)
                      ?1
                      :0.5,
                  child: Image.network(
                      tutor.imgUrl,
                      fit: BoxFit.fill,
                      width: 1000),
                ),

                GestureDetector(
                  onTap: (){
                    setState(() {
                      if(!widget.isHomePage) {
                        courseProvider.setSelectedTutor(index);
                      }
                    });
                  },
                  child: !widget.isHomePage? Container(
                    color: (courseProvider.selectedTutor == index)
                        ?Colors.black.withOpacity(0.7)
                        :Colors.transparent,
                    child: Center(
                      child: Container(
                        width: 100,
                        height: 100,
                        child: SvgPicture.asset(
                          'assets/images/course/check-48px.svg',
                          color: (courseProvider.selectedTutor==index)
                              ?Colors.white
                              :Colors.transparent,
                        ),
                      ),
                    ),
                  ):GestureDetector(
                    onTap: (){
                      widget.movePickPage('Tutor');
                    },
                  ),
                ),

                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(16, 0, 16, 32),
                    child: (index == _current)
                        ?Container(
                          height: 100,
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.7),
                            borderRadius: BorderRadius.circular(16)
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(16),
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    courseProvider.tutorParser.tutors[index].job,
                                    style: TextStyle(
                                      fontSize: 10,
                                      fontFamily: 'NotoSansCJKkr',
                                      fontWeight: FontWeight.w500,
                                      color: Color(0xff333333)
                                    ),
                                  ),
                                  Text(
                                    courseProvider.tutorParser.tutors[index].name,
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontFamily: 'NotoSansCJKkr',
                                      fontWeight: FontWeight.w700,
                                      color: Color(0xff333333)
                                    ),
                                  ),
                                  Container(
                                    width: size.width*0.2,
                                    height: 23,
                                    child: RaisedButton(
                                      elevation: 0,
                                      onPressed: (){},
                                      color: Colors.white,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(12)
                                      ),
                                      child: Text(
                                        'see more',
                                        style: TextStyle(
                                          fontSize: 10,
                                          fontFamily: 'NotoSansCJKkr',
                                          fontWeight: FontWeight.w500,
                                          color: Color(0xff333333)
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        )
                        :Container(
                          color: Colors.transparent,
                        ),
                  ),
                ),

                GestureDetector(
                  onTap: (){
                    if(!widget.isHomePage) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) =>
                              TutorDetail(tutorNum: index,))
                      );
                    }
                  },
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: !widget.isHomePage?Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 32),
                      child: Container(
                        height: 100,
                          decoration: BoxDecoration(
                              color: (courseProvider.selectedTutor==index)
                                  ?Colors.black.withOpacity(0.7)
                                  :Colors.transparent,
                              borderRadius: BorderRadius.circular(16)
                          )
                      ),
                    ):Container(),
                  ),
                )
              ],
            ),
          ),
        ),
      );
    }).toList();

    return tutorSliders;
  }

}
