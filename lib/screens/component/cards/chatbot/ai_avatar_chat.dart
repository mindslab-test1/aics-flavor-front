import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:heystars_app/services/common/font.dart';


Widget AiChatMessage(int currentStep, int currentIndex, String korText, String engText, Function playVideo){
  return Padding(
    padding: EdgeInsets.only(bottom: 4, left: 24),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          decoration: BoxDecoration(
              color: Color(0xfff4f4ff),
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30)
              )
          ),
          child: Padding(
            padding: EdgeInsets.fromLTRB(24, 16, 24, 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  //chatbotJson.introScenariosListStudy[currentStep].comScenario[currentIndex].korText,
                  korText,
                  style: TextStyle(
                      fontFamily: korFont,
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                      color: Color(0xff333333)
                  ),
                ),
                SizedBox(height: 4,),
                ConstrainedBox(
                  constraints: BoxConstraints(maxWidth: 200),
                  child: Text(
                    engText,
                    //chatbotJson.introScenariosListStudy[currentStep].comScenario[currentIndex].engText,
                    style: TextStyle(
                        fontFamily: nkorFont,
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                        color: Color(0xff333333)
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(width: 4,),
        GestureDetector(
          child: SvgPicture.asset(
            'assets/images/chatbot/play.svg',
            color: Color(0xff27d794),
          ),
          onTap: (){
            //playVideo(currentIndex);
            playVideo(currentStep, currentIndex, null, false);
          },
        )
      ],
    ),
  );
}

Widget StudentAnswerWidget(String kor, String otherLang){
  return Padding(
    padding: EdgeInsets.only(bottom: 4, right: 24),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          decoration: BoxDecoration(
              color: Color(0xff27d794),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                  bottomLeft: Radius.circular(30)
              )
          ),
          child: Padding(
            padding: EdgeInsets.fromLTRB(16, 24, 16, 24),
            child: Column(
              children: [
                Text(
                  kor,
                  style: TextStyle(
                      color: Colors.white
                  ),
                ),
                Text(
                  otherLang,
                  style: TextStyle(
                      color: Colors.white
                  ),
                )
              ],
            ),
          ),
        )
      ],
    ),
  );
}