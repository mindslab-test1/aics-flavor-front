import 'package:flutter/material.dart';
import 'package:heystars_app/services/common/font.dart';

class SpeakingContainer extends StatefulWidget {
  final String speak;
  final String speakPron;

  const SpeakingContainer({Key key, this.speak, this.speakPron}) : super(key: key);


  @override
  _SpeakingContainerState createState() => _SpeakingContainerState();
}

class _SpeakingContainerState extends State<SpeakingContainer> {



  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(top: 4, left: screenWidth * 0.178, bottom: 4),
          padding: EdgeInsets.only(top: 16, left: 24, right: 24, bottom: 16),
          decoration: BoxDecoration(
            color: Color.fromRGBO(244, 244, 255, 1),
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(36.0),
                bottomLeft: Radius.circular(36.0),
                bottomRight: Radius.circular(36.0)),
          ),
          child: Column(
            children: [Text(widget.speak,
              style: msgStyle,
            ),
              SizedBox(
                height: 4,
              ),
              if(widget.speakPron !=null)Text(widget.speakPron,
                style: pronStyle, )],
          ),),
      ],
    );
  }
}
