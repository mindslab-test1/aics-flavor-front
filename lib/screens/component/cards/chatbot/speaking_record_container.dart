import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:heystars_app/providers/scenario_list_status.dart';
import 'package:heystars_app/services/audio_recorder/flutter_audio_recorder.dart';
import 'package:heystars_app/services/audio_recorder/recorder.dart';
import 'package:file/local.dart';
import 'package:heystars_app/services/json_parser/chatbot/stt_json.dart';
import 'package:heystars_app/services/repositories/stt_repository.dart';
import 'package:provider/provider.dart';

class SpeakingRecordContainer extends StatefulWidget {
  Function addMessage;
  Function checkMessage;

  SpeakingRecordContainer({
    Key key,
    this.addMessage,
    this.checkMessage,
  }) : super(key: key);

  @override
  _SpeakingRecordContainerState createState() =>
      _SpeakingRecordContainerState();
}

class _SpeakingRecordContainerState extends State<SpeakingRecordContainer> {
  ScenarioListStatus scenarioListStatus;
  Icon icon = Icon(
    Icons.mic,
    color: Colors.white,
  );

  Recorder recorder;
  LocalFileSystem localFileSystem;
  SttRes sttRes;

  // 다음 문제로 넘기기위함
  bool clicked = false;
  // 다음 문제가 나옴을 체크
  bool problemUp = false;

  @override
  void initState() {
    super.initState();
    localFileSystem = LocalFileSystem();
    recorder = Recorder(localFileSystem: localFileSystem);
    scenarioListStatus =
        Provider.of<ScenarioListStatus>(context, listen: false);
    initRecord();
  }

  // 화면 전환 전 recoder종료
  @override
  void dispose(){
    recorder.dispose();
    super.dispose();
  }

  initRecord() async {
    await recorder.init();
    setState(() {
      icon = changeIcon();
    });
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;

    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              width: screenWidth * 0.233,
              margin:
              EdgeInsets.only(top: 4, left: screenWidth * 0.178, bottom: 4),
              padding: EdgeInsets.only(top: 16, left: 24, right: 24, bottom: 16),
              decoration: BoxDecoration(
                color: Color.fromRGBO(244, 244, 255, 1),
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(36.0),
                    bottomLeft: Radius.circular(36.0),
                    bottomRight: Radius.circular(36.0)),
              ),
              child: GestureDetector(
                  onTap: () async {
                    if(problemUp ==false){
                      await recorder.changeRecordStatus();
                      setState(() {
                        icon = changeIcon();;
                      });

                      if (recorder.currentStatus == RecordingStatus.Stopped) {
                        final res = await SttRepository().callSttApi(recorder.file);
                        sttRes = SttRes.fromJson(json.decode(res.toString()));
                        sttRes.data = sttRes.data.replaceAll("\n", "");
                        widget.addMessage(sttRes.data, '');
                        checkPron();
                        problemUp = true;
                        await recorder.init();
                    }
                      }
                  },
                  child: CircleAvatar(
                      backgroundColor: Color.fromRGBO(39, 215, 148, 1),
                      child: icon))),
          Material(
            color: Colors.transparent,
            child: InkWell(
              splashColor: Color.fromRGBO(237, 237, 237, 1),
              child: Container(
                width: screenWidth * 0.125,
                margin: EdgeInsets.only(top: 4, bottom: 4, left: 4),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text('SKIP',
                  style: TextStyle(
                    fontSize: 10,
                    fontFamily: 'NotoSansCJKkr',
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(51, 51, 51, 1),
                    decoration: TextDecoration.underline
                  ),),
                ),
              ),
                onTap: () {
                  setState(() {
                    if(problemUp==false){
                      clicked = !clicked;
                      if(clicked) {
                        widget.addMessage(scenarioListStatus.userAns, '');
                        widget.checkMessage('Reading');
                        problemUp = true;
                      }
                    }
                  });
                }
            ),
          ),
        ],
      )
    );
  }

  Icon changeIcon() {
    if (recorder.currentStatus == RecordingStatus.Initialized) {
      return Icon(
        Icons.mic,
        color: Colors.white,
      );
    } else if (recorder.currentStatus == RecordingStatus.Recording) {
      return Icon(
        Icons.stop,
        color: Colors.white,
      );
    }
    return Icon(
      Icons.mic,
      color: Colors.white,
    );
  }

  // 발음의 정답 유무 확인
  checkPron() {
    if (sttRes.data == scenarioListStatus.userAns) {
      widget.checkMessage('Reading');
    } else if (sttRes.data == '') {
      widget.checkMessage('N');
    } else {
      widget.checkMessage('W');
    }
  }
}
