
import 'package:flutter/material.dart';
import 'package:heystars_app/providers/scenario_list_status.dart';
import 'package:heystars_app/screens/component/button/selected_btn.dart';
import 'package:provider/provider.dart';

class SelectedBtnCard extends StatefulWidget {
  final List<String> choices;
  final List<String> choicesPron;
  Function addMessage;
  Function responseMessage;
  String scenarioName;

 SelectedBtnCard({Key key, this.choices, this.choicesPron, this.addMessage, this.responseMessage, this.scenarioName,}) : super(key: key);

  @override
  _SelectedBtnCardState createState() => _SelectedBtnCardState();
}

class _SelectedBtnCardState extends State<SelectedBtnCard> {
  int selectedIdx;
  bool clicked = false;


  ScenarioListStatus scenarioListStatus;

  @override
  void initState(){
    super.initState();
    scenarioListStatus = Provider.of<ScenarioListStatus>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return Row(
      children: [
        IntrinsicWidth(
          child: Container(
            margin: EdgeInsets.only(top: 4, left: screenWidth * 0.178, bottom: 4),
            child: IntrinsicWidth(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  SelectedBtn(idx: 0, ans: widget.choices[0], ansPron: widget.choicesPron[0], selectedIdx: selectedIdx, changeIdx: changeIdx, topleft: 0, topright: 36.0, bottomleft: 0, bottomright: 0, clicked: clicked, scenarioName: widget.scenarioName,),
                  SelectedBtn(idx: 1, ans: widget.choices[1], ansPron: widget.choicesPron[1], selectedIdx: selectedIdx, changeIdx: changeIdx, topleft: 0, topright: 0, bottomleft: 0, bottomright: 0,clicked: clicked,scenarioName: widget.scenarioName, ),
                  SelectedBtn(idx: 2, ans: widget.choices[2], ansPron: widget.choicesPron[2], selectedIdx: selectedIdx, changeIdx: changeIdx, topleft: 0, topright: 0, bottomleft: 0, bottomright: 0,clicked: clicked,scenarioName: widget.scenarioName,),
                  SelectedBtn(idx: 3, ans: widget.choices[3], ansPron: widget.choicesPron[3], selectedIdx: selectedIdx, changeIdx: changeIdx, topleft: 0, topright: 0, bottomleft: 36.0, bottomright: 36.0,clicked: clicked,scenarioName: widget.scenarioName,)
                ],
              ),
            ),
          ),
        )
      ],
    );
}

  changeIdx(int idx) {
    setState(() {
      selectedIdx = idx;
      clicked = !clicked;
    });
    widget.addMessage(widget.choices[idx], widget.choicesPron[idx]);
    scenarioListStatus.setUserAns(widget.choices[idx]);
    if (scenarioListStatus.scenarioList[scenarioListStatus.index][idx+1].response == 'Try again') {
      widget.responseMessage(
          scenarioListStatus.scenarioList[scenarioListStatus.index][idx+1].responseKor,
          scenarioListStatus.scenarioList[scenarioListStatus.index][idx+1].response,
          '',false);
    } else {
      widget.responseMessage(
          scenarioListStatus.scenarioList[scenarioListStatus.index][idx+1].responseKor,
          scenarioListStatus.scenarioList[scenarioListStatus.index][idx+1].response,
          scenarioListStatus.scenarioList[scenarioListStatus.index][idx+1].mediaUrl,
          true);
    }
  }
}
