import 'package:flutter/material.dart';

class QuestionImageContainer extends StatelessWidget {
  final String path;


  QuestionImageContainer({Key key, this.path}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery
        .of(context)
        .size
        .width;

    return Flexible(
        child: Container(
          width: screenWidth * 0.531,
          margin: EdgeInsets.only(top: 4, bottom: 4, left: screenWidth * 0.178),
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
                topRight: Radius.circular(36.0),
                bottomLeft: Radius.circular(36.0),
                bottomRight: Radius.circular(36.0)
            ),
          ),
          child: ClipRRect(
            borderRadius: const BorderRadius.only(
                topRight: Radius.circular(36.0),
                bottomLeft: Radius.circular(36.0),
                bottomRight: Radius.circular(36.0)
            ),
            child: Image.network(
              path,
              loadingBuilder: (BuildContext context, Widget child,
                  ImageChunkEvent loadingProgress) {
                if (loadingProgress == null) return child;
                return Container(
                    width: 100,
                    height: 100,
                    child: Center(
                      child: CircularProgressIndicator(
                        value: loadingProgress.expectedTotalBytes != null
                            ? loadingProgress.cumulativeBytesLoaded /
                            loadingProgress.expectedTotalBytes
                            : null,
                      ),
                    )
                );
              },
            ),
          ),
          //   ],
          // ),
        )
    );
  }

}
