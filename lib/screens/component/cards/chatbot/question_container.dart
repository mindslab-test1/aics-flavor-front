import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:heystars_app/services/common/font.dart';

class QuestionContainer extends StatefulWidget {
  final String question;
  final String questionPron;
  final String mediaUrl;
  final String scenarioName;

  QuestionContainer({Key key, this.question, this.questionPron, this.mediaUrl, this.scenarioName}) : super(key: key);

  @override
  _QuestionContainerState createState() => _QuestionContainerState();
}

class _QuestionContainerState extends State<QuestionContainer> {



  bool play = false;
  AudioPlayer player = AudioPlayer();

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;

    
    return Row(
      children: [
        Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: screenWidth* 0.1,
            margin: EdgeInsets.only(top:8, left: screenWidth * 0.067),
            child: CircleAvatar(
              backgroundImage: AssetImage('assets/images/chatbot/profile_eunjung.png'),
              backgroundColor: Colors.transparent,
            ),),
          Container(
            constraints: BoxConstraints(maxWidth: screenWidth * 0.531),
            margin: EdgeInsets.only(bottom: 4, top:8, left: 4),
            padding: EdgeInsets.only(top: 16, left: 24, right: 24, bottom: 16),
            decoration: BoxDecoration(
              color: Color.fromRGBO(244, 244, 255, 1),
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(36.0),
                  bottomLeft: Radius.circular(36.0),
                  bottomRight: Radius.circular(36.0)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [Text(widget.question,
              style: msgStyle
              ),
                SizedBox(
                  height: 4,
                ),
                Text(widget.questionPron,
                style: pronStyle, )],
            ),),
        ],
      ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            widget.scenarioName == 'study' || widget.mediaUrl ==null?Container()
                :Container(
                child : IconButton(
                  icon: SvgPicture.asset(play?'assets/images/chatbot/stop.svg':'assets/images/chatbot/play.svg'),
                  onPressed: (){
                    setState(() {
                      play = !play;
                      if (play == true){
                        player.play(widget.mediaUrl);
                        player.onPlayerCompletion.listen((event) {
                          setState(() {
                            play = false;
                          });
                        });
                      }else{
                        player.stop();
                      }
                    });
                  },
                )
            )
          ],
        )
    ]
    );
  }
}
