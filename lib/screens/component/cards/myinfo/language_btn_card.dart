import 'package:flutter/material.dart';
import 'package:heystars_app/providers/myinfo_provider.dart';
import 'package:heystars_app/providers/user_provider.dart';
import 'package:heystars_app/screens/component/button/selected_choice_btn.dart';
import 'package:heystars_app/services/repositories/myinfo_shared_preferences.dart';
import 'package:heystars_app/services/repositories/user_repository.dart';
import 'package:provider/provider.dart';

class LanguageBtnCard extends StatefulWidget {
  final List<String> names;
  final List<String> onImgs;
  final List<String> offImgs;
  final String category;

  const LanguageBtnCard({Key key, this.names, this.onImgs, this.offImgs, this.category, }) : super(key: key);


  @override
  _LanguageBtnCardState createState() => _LanguageBtnCardState();
}

class _LanguageBtnCardState extends State<LanguageBtnCard> {
  MyInfoProvider myInfoProvider;
  int selectedIdx;
  SharedPrefs sharedPrefs = SharedPrefs();
  UserProvider userProvider;



  @override
  Widget build(BuildContext context) {
    selectedIdx = sharedPrefs.languageIndex;
    myInfoProvider = Provider.of<MyInfoProvider>(context);
    userProvider = Provider.of<UserProvider>(context);

    return Column(
      children: [
        SelectedChoiceBtn(idx: 0, name: widget.names[0], selectedIdx: selectedIdx, changeIdx: changeIdx, onImg:widget.onImgs[0], offImg: widget.offImgs[0], category: widget.category,),
        Divider(
          thickness: 1,
        ),
        SelectedChoiceBtn(idx: 1, name: widget.names[1], selectedIdx: selectedIdx, changeIdx: changeIdx, onImg:widget.onImgs[1], offImg: widget.offImgs[1], category: widget.category,),
        Divider(
          thickness: 1,
        ),
        SelectedChoiceBtn(idx: 2, name: widget.names[2], selectedIdx: selectedIdx, changeIdx: changeIdx, onImg:widget.onImgs[2], offImg: widget.offImgs[2], category: widget.category,),
        Divider(
          thickness: 1,
        ),
      ],
    );
  }

  changeIdx(int idx){
    setState(() {
       selectedIdx = idx;
       sharedPrefs.setLanguageIndex = selectedIdx;
       sharedPrefs.setLanguageName = widget.names[selectedIdx];
       sharedPrefs.setLanguageNation = widget.names[selectedIdx];
       sharedPrefs.setLanguageImg = widget.onImgs[selectedIdx];
       myInfoProvider.setLanguageImg(widget.onImgs[selectedIdx]);
       myInfoProvider.setLanguageName(widget.names[selectedIdx]);
       myInfoProvider.setLanguageNation(widget.names[selectedIdx]);
       putUserLang();
    });
  }

  // user provider의 이름으로 값변경
  updateLange(String lang){
    if(lang =='English'){
      return 'Eng';
    }else if(lang =='Indonesian'){
      return 'Indo';
    }else{
      return 'Viet';
    }
  }

  // status 상태에 따라 데이터 변경 저장
  Future<void> putUserLang() async{
    final response = await UserRepository().putUserLang(userProvider.uid, updateLange(widget.names[selectedIdx]));
    if(response ==200){
      userProvider.setLang(updateLange(widget.names[selectedIdx]));
    }else{
      return response;
    }
  }
}

