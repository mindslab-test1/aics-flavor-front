import 'package:flutter/material.dart';
import 'package:heystars_app/providers/myinfo_provider.dart';
import 'package:heystars_app/screens/component/button/selected_choice_btn.dart';
import 'package:heystars_app/services/repositories/myinfo_shared_preferences.dart';
import 'package:provider/provider.dart';

class VoiceBtnCard extends StatefulWidget {
  final List<String> names;
  final List<String> onImgs;
  final List<String> offImgs;
  final String category;

  const VoiceBtnCard({Key key, this.names, this.onImgs, this.offImgs, this.category}) : super(key: key);

  @override
  _VoiceBtnCardState createState() => _VoiceBtnCardState();
}

class _VoiceBtnCardState extends State<VoiceBtnCard> {
  int selectedIdx;
  MyInfoProvider myInfoProvider;
  SharedPrefs sharedPrefs = SharedPrefs();

  @override
  Widget build(BuildContext context) {
    myInfoProvider = Provider.of<MyInfoProvider>(context);
    selectedIdx = sharedPrefs.voiceIndex;

    return Column(
      children: [
        SelectedChoiceBtn(idx: 0, name: widget.names[0], selectedIdx: selectedIdx, changeIdx: changeIdx, onImg:widget.onImgs[0], offImg: widget.offImgs[0], category: widget.category,),
        Divider(
          thickness: 1,
        ),
        SelectedChoiceBtn(idx: 1, name: widget.names[1], selectedIdx: selectedIdx, changeIdx: changeIdx, onImg:widget.onImgs[1], offImg: widget.offImgs[1], category: widget.category,),
        Divider(
          thickness: 1,
        ),
        SelectedChoiceBtn(idx: 2, name: widget.names[2], selectedIdx: selectedIdx, changeIdx: changeIdx, onImg:widget.onImgs[2], offImg: widget.offImgs[2], category: widget.category,),
        Divider(
          thickness: 1,
        ),
      ],
    );
  }

  changeIdx(int idx){
    setState(() {
      selectedIdx = idx;
      sharedPrefs.setVoiceIndex = selectedIdx;
      sharedPrefs.setVoiceName = widget.names[selectedIdx];
      sharedPrefs.setVoiceImg = widget.onImgs[selectedIdx];
      myInfoProvider.setVoiceIndex(selectedIdx);
      myInfoProvider.setVoiceName(widget.names[selectedIdx]);
      myInfoProvider.setVoiceImg(widget.onImgs[selectedIdx]);
    });
  }
}
