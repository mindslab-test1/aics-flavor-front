import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:heystars_app/screens/layouts/myinfo_page/alarm_voice_page.dart';
import 'package:heystars_app/screens/layouts/myinfo_page/cscenter_page.dart';
import 'package:heystars_app/screens/layouts/myinfo_page/notice_page.dart';
import 'package:heystars_app/screens/layouts/myinfo_page/payment_page.dart';

class InfoMenuCard extends StatefulWidget {
  final String menuName;
  final String content;

  const InfoMenuCard({Key key, this.menuName, this.content,}) : super(key: key);
  @override
  _InfoMenuCardState createState() => _InfoMenuCardState();
}

class _InfoMenuCardState extends State<InfoMenuCard> {


  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return Container(
      height: screenHeight * 0.063,
      margin: EdgeInsets.only(bottom: 4),
      child: Row(
        children: [
          VerticalDivider(
            thickness: 2,
            color: Color.fromRGBO(39, 215, 148, 1),
          ),
          SizedBox(
            width: screenWidth * 0.051,
          ),
          Text(widget.menuName,
            style: TextStyle(
                fontSize: 14,
                fontFamily: 'NotoSansCJKkr',
                fontWeight: FontWeight.w500
            ),),
          Container(
            margin: EdgeInsets.only(left: screenWidth * 0.038),
            child: Text(widget.menuName =='Voice'
                ?widget.content != null? '[' + '${widget.content}' + ']':'[Eunjung]'
            :'',
              style: TextStyle(
                  fontSize: 14,
                  fontFamily: 'NotoSansCJKkr',
                  fontWeight: FontWeight.w400,
                  color: Color.fromRGBO(39, 215, 148, 1)
              ),)
          ),
          Expanded(
            child: Container(),
          ),
          Container(
            margin: EdgeInsets.only(right: screenWidth * 0.033),
            child: InkWell(
              child: SvgPicture.asset('assets/images/myinfo/arrow.svg'),
              onTap: (){
                if(widget.menuName == 'CS Center'){
                  Navigator.push(context, MaterialPageRoute(builder:(BuildContext context) => CsCenterPage()));
                }else if(widget.menuName == 'Notice'){
                  Navigator.push(context, MaterialPageRoute(builder:(BuildContext context) => NotciePage()));
                }else if(widget.menuName == 'Payment'){
                  Navigator.push(context, MaterialPageRoute(builder:(BuildContext context) => PaymentPage()));
                }else{
                  Navigator.push(context, MaterialPageRoute(builder:(BuildContext context) => VoicePage()));
                }
              }
            )
          )
        ],
      ),
    );
  }
}
