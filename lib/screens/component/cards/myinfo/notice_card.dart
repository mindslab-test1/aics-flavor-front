import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NoticeCard extends StatefulWidget {
  final String title;
  final String date;
  final String content;
  final bool upload;

  const NoticeCard({Key key, this.title, this.date, this.content, this.upload}) : super(key: key);

  @override
  _NoticeCardState createState() => _NoticeCardState();
}

class _NoticeCardState extends State<NoticeCard> {

  bool viewMore = false;

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    final screenHeight = MediaQuery
        .of(context)
        .size
        .height;

    return
      Container(
        padding: EdgeInsets.only(left: screenWidth * 0.067, right: screenWidth * 0.067),
        child: Column(
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                      child: Container(
                        padding: EdgeInsets.all(screenWidth * 0.038),
                        margin: EdgeInsets.only(right: 4),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              widget.upload ==true
                                  ?Badge(
                                badgeContent: Text('NEW',
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontFamily: 'NotoSansCJKkr',
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white
                                  ),),
                                badgeColor: Color.fromRGBO(39, 215, 148, 1),
                                shape: BadgeShape.square,
                              ):Container(),
                              Container(
                                  margin: EdgeInsets.only(top : 3, bottom: 2),
                                  child: Text(widget.title,
                                    softWrap: true,
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: 'NotoSansCJKkr',
                                        fontWeight: FontWeight.w500,
                                        color: Color.fromRGBO(51, 51, 51, 1)
                                    ),)
                              ),
                              Container(
                                child: Text(
                                  widget.date,
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontFamily: 'NotoSansCJKkr',
                                      fontWeight: FontWeight.w400,
                                      color: Color.fromRGBO(153, 153, 153, 1)
                                  ),
                                ),
                              ),
                            ],
                          )
                      )),
                  GestureDetector(
                    onTap: (){
                      setState(() {
                        viewMore = !viewMore;
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.only(top : screenWidth * 0.038),
                      child: SvgPicture.asset('assets/images/myinfo/ico_arrorw_down-32px.svg'),
                    ),
                  )
                ],
              )
            ),
            if (viewMore == true) Visibility(
              visible: viewMore ==true,
              child: Container(
                width: screenWidth * 0.867,
                padding: EdgeInsets.all(screenWidth * 0.038),
                margin: EdgeInsets.only(bottom: 1),
                color: Color.fromRGBO(252, 252, 252, 1),
                // color: Colors.grey,
                child: Text(
                widget.content,
                ),
              ),
              ),
          Divider(
              height: 1,
              color: Color.fromRGBO(245, 245, 245, 1),
            ),
          ],
        ),
      );
  }
}
