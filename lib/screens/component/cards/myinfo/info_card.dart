import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:heystars_app/providers/myinfo_provider.dart';
import 'package:heystars_app/providers/user_provider.dart';
import 'package:heystars_app/screens/component/cards/myinfo/week_choice_card.dart';
import 'package:heystars_app/screens/layouts/myinfo_page/alarm_page.dart';
import 'package:heystars_app/screens/layouts/myinfo_page/language_page.dart';
import 'package:heystars_app/services/repositories/myinfo_shared_preferences.dart';
import 'package:provider/provider.dart';

class InfoCard extends StatefulWidget {
  final String cardName;
  final String imgPath;
  final bool imgShape;
  final double imgWidth;
  final double imgHeight;
  final String imgName;
  final String cardInfo;

  InfoCard({Key key, this.cardName, this.imgPath, this.imgName, this.cardInfo, this.imgShape, this.imgWidth, this.imgHeight}) : super(key: key);


  @override
  _InfoCardState createState() => _InfoCardState();
}

class _InfoCardState extends State<InfoCard> {
  SharedPrefs sharedPrefs = SharedPrefs();
  List<bool> days = List();

  // sharedPrefs에 저장된 요일 체크를 provider에 저장하기 위해 타입 변경
  changeBool(){
    days.clear();
    sharedPrefs.alarmDaysCheck.forEach((element) {
      if(element =="1"){
        days.add(true);
      }else{
        days.add(false);
      }
    });
  }

  @override
  Widget build(BuildContext context) {

    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    MyInfoProvider myInfoProvider = Provider.of<MyInfoProvider>(context);
    changeBool();

    //sharedPrefs의 저장된 내용을 provider 초기값으로 load
    myInfoProvider.loadAlarmDaysCheck(days);

    UserProvider userProvider = Provider.of<UserProvider>(context, listen: false);

    return InkWell(
      onTap: (){
        if(widget.cardName =='Language'){
          Navigator.push(context, MaterialPageRoute(builder:(BuildContext context) => LanguagePage()));
        }else{
          Navigator.push(context, MaterialPageRoute(builder:(BuildContext context) => AlarmPage()));
        }
      },
      child: langCard(screenWidth, screenHeight, userProvider),
      // child: Container(
      //   padding: EdgeInsets.only(left: screenWidth * 0.044, right: screenWidth * 0.044, top: screenHeight * 0.021, bottom: screenHeight * 0.021),
      //   width: screenWidth * 0.417,
      //   decoration: BoxDecoration(
      //       borderRadius: BorderRadius.circular(8),
      //       border: Border.all(
      //           color:  sharedPrefs.alarmCheck == true || widget.cardName != 'Alarm'?Color.fromRGBO(39, 215, 148, 1): Color.fromRGBO(213, 213, 213, 1), width: 1)
      //   ),
      //   child: Column(
      //     crossAxisAlignment: CrossAxisAlignment.start,
      //     children: [
      //       Container(
      //         height: 24,
      //         child: Row(
      //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //           children: [
      //             Text(widget.cardName,
      //               style: TextStyle(
      //                   fontSize: 12,
      //                   fontFamily: 'NotoSansCJKkr',
      //                   fontWeight: FontWeight.w400
      //               ),),
      //             widget.cardName == 'Alarm'
      //                 ?Transform.scale(scale: 0.9,
      //               child: CupertinoSwitch(value: sharedPrefs.alarmCheck, onChanged: (bool value){
      //                 setState(() {
      //                   sharedPrefs.setAlarmCheck = value;
      //                 });
      //               },
      //                 activeColor: Color.fromRGBO(39, 215, 148, 1),),):Container()
      //           ],
      //         ),
      //       ),
      //       SizedBox(
      //         height: 8,
      //       ),
      //         Row(
      //           children: [
      //             widget.imgShape == true
      //                 ? Container(
      //                     margin: EdgeInsets.only(right: 4),
      //                     width: widget.imgWidth,
      //                     height: widget.imgHeight,
      //                     child: CircleAvatar(
      //                       backgroundImage: widget.imgPath != null
      //                           ? AssetImage(widget.imgPath)
      //                           : AssetImage(
      //                               'assets/images/chatbot/profile_eunjung.png'),
      //                     ),
      //                   )
      //                 : Container(
      //                     margin: EdgeInsets.only(right: 4),
      //                     width: widget.imgWidth,
      //                     height: widget.imgHeight,
      //                     child: widget.imgPath != null
      //                         ? SvgPicture.asset(
      //                             widget.imgPath,
      //                           )
      //                         : SvgPicture.asset(
      //                             changeLangImg(changeLang(userProvider.lang))),
      //                   ),
      //             widget.cardName =='Alarm'?
      //             Text(widget.imgName !=null?widget.imgName:'Eunjung',
      //               style: TextStyle(
      //                   fontSize: 10,
      //                   fontFamily: 'NotoSansCJKkr',
      //                   fontWeight: FontWeight.w400
      //               ),
      //               textAlign: TextAlign.left,):Container()
      //           ],
      //         ),
      //       widget.cardName =='Alarm'?
      //       Text(widget.cardInfo !=null?widget.cardInfo:'no setting',
      //         style: TextStyle(
      //             fontSize: 18,
      //             fontFamily: 'NotoSansCJKkr',
      //             fontWeight: FontWeight.w700
      //         ),
      //         textAlign: TextAlign.center,)
      //           :Text(widget.cardInfo !=null?widget.cardInfo:changeLang(userProvider.lang),
      //         style: TextStyle(
      //             fontSize: 18,
      //             fontFamily: 'NotoSansCJKkr',
      //             fontWeight: FontWeight.w700
      //         ),
      //         textAlign: TextAlign.center,),
      //
      //      widget.cardName != 'Alarm'
      //       ?Text(widget.imgName !=null?widget.imgName:changeLangNation(changeLang(userProvider.lang)),
      //         style: TextStyle(
      //             fontSize: 10,
      //             fontFamily: 'NotoSansCJKkr',
      //             fontWeight: FontWeight.w400
      //         ),
      //         textAlign: TextAlign.left,)
      //          :WeekChoiceCard()
      //     ],
      //   ),
      // ),
    );
  }

  // Full lang name
  changeLang(String name){
    if(name =='Eng'){
      return 'English';
    }else if (name =='Indo'){
      return 'Indonesian';
    }else{
      return 'Vietnamese';
    }
  }

  //lang img
  changeLangImg(String name){
    if(name =='English'){
      return 'assets/images/myinfo/free-icon-united-states-of-america-940207.svg';
    }else if(name =='Indonesian'){
      return 'assets/images/myinfo/free-icon-indonesia.svg';
    }else{
      return 'assets/images/myinfo/free-icon-vietnam-940212.svg';
    }
  }

  //lang nation
  changeLangNation(String name){
    if(name =='English'){
      return 'U.S.A';
    }else if(name =='Indonesian'){
      return 'Indonesia';
    }else{
      return 'Vietnam';
    }
  }
  
  //pre_myinfo
  Widget langCard(screenWidth, screenHeight, userProvider){
    return Container(
      padding: EdgeInsets.only(left: screenWidth * 0.044, right: screenWidth * 0.044, top: screenHeight * 0.021, bottom: screenHeight * 0.021),
      width: screenWidth * 0.867,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
              color:  sharedPrefs.alarmCheck == true || widget.cardName != 'Alarm'?Color.fromRGBO(39, 215, 148, 1): Color.fromRGBO(213, 213, 213, 1), width: 1)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.cardName,
            style: TextStyle(
                fontSize: 12,
                fontFamily: 'NotoSansCJKkr',
                fontWeight: FontWeight.w400
            ),),
          SizedBox(
            height: 4,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(widget.cardInfo !=null?widget.cardInfo:changeLang(userProvider.lang),
                style: TextStyle(
                    fontSize: 18,
                    fontFamily: 'NotoSansCJKkr',
                    fontWeight: FontWeight.w700
                ),
                textAlign: TextAlign.center,),
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 4),
                    width: widget.imgWidth,
                    height: widget.imgHeight,
                    child: widget.imgPath != null
                        ? SvgPicture.asset(
                      widget.imgPath,
                    )
                        : SvgPicture.asset(
                        changeLangImg(changeLang(userProvider.lang))),
                  ),
                  Text(widget.imgName !=null?widget.imgName:changeLangNation(changeLang(userProvider.lang)),
                    style: TextStyle(
                        fontSize: 10,
                        fontFamily: 'NotoSansCJKkr',
                        fontWeight: FontWeight.w400
                    ),
                    textAlign: TextAlign.left,)
                ],
              ),
            ],
          )
        ],
      ),
    );
  }

}

