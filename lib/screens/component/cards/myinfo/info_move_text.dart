import 'package:flutter/material.dart';

class InfoMoveText extends StatefulWidget {
  final String moveName;

  const InfoMoveText({Key key, this.moveName}) : super(key: key);

  @override
  _InfoMoveTextState createState() => _InfoMoveTextState();
}

class _InfoMoveTextState extends State<InfoMoveText> {
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    return Container(
      alignment: Alignment.centerRight,
      margin: EdgeInsets.only(right: screenWidth * 0.067),
      padding: EdgeInsets.only(top: screenHeight*0.011, left: screenWidth * 0.033, right: screenWidth * 0.033, bottom: screenHeight * 0.013),
      child: Text(widget.moveName,
        style: TextStyle(
            fontSize: 12,
            fontFamily: 'NotoSansCJKkr',
            fontWeight: FontWeight.w500,
            decoration: TextDecoration.underline
        ),),
    );
  }
}
