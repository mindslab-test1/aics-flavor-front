import 'package:flutter/material.dart';
import 'package:heystars_app/screens/component/button/week_choice_btn.dart';

class WeekChoiceCard extends StatefulWidget {
  @override
  _WeekChoiceCardState createState() => _WeekChoiceCardState();
}

class _WeekChoiceCardState extends State<WeekChoiceCard> {

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;

    return Container(
      width: screenWidth * 0.417 *0.46,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          WeekChoiceBtn(day: 'S', idx: 0,),
          WeekChoiceBtn(day: 'M', idx: 1,),
          WeekChoiceBtn(day: 'T', idx: 2,),
          WeekChoiceBtn(day: 'W', idx: 3,),
          WeekChoiceBtn(day: 'T', idx: 4,),
          WeekChoiceBtn(day: 'F', idx: 5,),
          WeekChoiceBtn(day: 'S', idx: 6,),
        ],
      ),
    );
  }
}
