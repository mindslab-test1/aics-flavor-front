import 'package:flutter/material.dart';

class PaymentCard extends StatefulWidget {
  @override
  _PaymentCardState createState() => _PaymentCardState();
}

class _PaymentCardState extends State<PaymentCard> {
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;

    return Column(
        children: [
          Container(
              width: screenWidth * 0.867,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Color.fromRGBO(244, 244, 244, 1)
              ),
              child: Container(
                margin: EdgeInsets.only(left: screenWidth * 0.051, top: 16, bottom: 16),
                child : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('ALL PASS',
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'NotoSansCJKkr',
                          fontWeight: FontWeight.w500,
                          color: Color.fromRGBO(51, 51, 51, 1)
                      ),),
                    Container(
                        margin: EdgeInsets.only(top: 4),
                        width: screenWidth * 0.176,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: Color.fromRGBO(213, 213, 213, 1)
                        ),
                        child: Center(
                          child: Text(
                            '\$10.00',
                            style: TextStyle(
                              fontSize: 12,
                              fontFamily: 'NotoSansCJKkr',
                              fontWeight: FontWeight.w400,
                              color: Colors.white,
                            ),
                          ),
                        )
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 4,),
                      child: Text('2020.01.01 - 2020.01.31',
                        style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'NotoSansCJKkr',
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(153, 153, 153, 1)
                        ),),
                    )
                  ],
                ),
              )
          )
        ],
      );
  }
}
