import 'package:flutter/material.dart';
import 'package:heystars_app/providers/myinfo_provider.dart';
import 'package:heystars_app/services/repositories/myinfo_shared_preferences.dart';
import 'package:provider/provider.dart';

class AlarmDayBtn extends StatefulWidget {
  final String short;
  final int idx;
  final String full;

  const AlarmDayBtn({Key key, this.short, this.idx, this.full}) : super(key: key);
  @override
  _AlarmDayBtnState createState() => _AlarmDayBtnState();
}

class _AlarmDayBtnState extends State<AlarmDayBtn> {
  bool check;
  MyInfoProvider myInfoProvider;
  SharedPrefs sharedPrefs = SharedPrefs();


  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    MyInfoProvider myInfoProvider = Provider.of<MyInfoProvider>(context);
    check = myInfoProvider.alarmDaysCheck[widget.idx];

    //sharedPrefs에 요일 선택저장
    saveDaysCheck(){
      List<String> daysCheck = List();
      myInfoProvider.alarmDaysCheck.forEach((element) {
        if(element){
          daysCheck.add("1");
        }else{
          daysCheck.add("0");
        }
      });
      sharedPrefs.setAlarmDaysCheck = daysCheck;
    }

    return Container(
      width: screenWidth * 0.115,
      child: RaisedButton(
        elevation: 0,
        shape: CircleBorder(),
        color: check? Color.fromRGBO(39, 215, 148, 1): Colors.white,
        child: Text('${widget.short}',
        style: TextStyle(
          fontSize: 13,
          fontFamily: 'NotoSansCJKkr',
          fontWeight: FontWeight.w500,
          color: check?Colors.white : Color.fromRGBO(51, 51, 51, 1)
        ),
        textAlign: TextAlign.center,),
        onPressed: (){
          // myInfoProvider.popAlarmDays('Day');
          setState(() {
            myInfoProvider.setAlarmDaysCheck(widget.idx);
          });
          // if(myInfoProvider.alarmDaysCheck[widget.idx]){
          //   myInfoProvider.setAlarmDays(widget.full);
          // }else{
          //   myInfoProvider.popAlarmDays(widget.full);
          // }
          // if(myInfoProvider.alarmDays.length == 0){
          //   myInfoProvider.setAlarmDays('Day');
          // }
          saveDaysCheck();
        },

      ),
    );

  }


}
