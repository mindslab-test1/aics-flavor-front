import 'package:flutter/material.dart';
import 'package:heystars_app/services/common/font.dart';

class SelectedBtn extends StatelessWidget {
  final int selectedIdx;
  final int idx;
  final String ans;
  final String ansPron;
  final Function changeIdx;
  final double topleft;
  final double topright;
  final double bottomleft;
  final double bottomright;
  final bool clicked;
  final String scenarioName;

  SelectedBtn({
    this.idx,
    this.ans,
    this.ansPron,
    this.selectedIdx,
    this.changeIdx,
    this.topleft,
    this.topright,
    this.bottomleft,
    this.bottomright,
    this.clicked,
    this.scenarioName,
  });


  @override
  Widget build(BuildContext context) {

    return GestureDetector(
        onTap: () =>
        {
          if (clicked == false){
            changeIdx(idx),
          }
        },
        child: Container(
          //width: screenWidth * size,
          padding: EdgeInsets.only(top: 12, left: 24, bottom: 12, right: 24),
          decoration: BoxDecoration(
              color: selectedIdx == idx
                  ? Color.fromRGBO(39, 215, 148, 1)
                  : Color.fromRGBO(
                  244, 244, 255, 1),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(topleft),
                  topRight: Radius.circular(topright),
                  bottomLeft: Radius.circular(bottomleft),
                  bottomRight: Radius.circular(bottomright)
              )
          ),
          child: Row(
            children: [
              FittedBox(
                fit: BoxFit.cover,
                child: Text("${ans}",
                  style: msgStyle.copyWith(color: selectedIdx==idx?Colors.white:Color.fromRGBO(51, 51, 51, 1)),
                ),
              ),
              if(scenarioName == 'study') SizedBox(width: 8,),
              if(scenarioName == 'study')
              FittedBox(
                  fit: BoxFit.cover,
                  child: Text("[${ansPron}]",
                      style: pronStyle.copyWith(color: selectedIdx==idx?Colors.white:Color.fromRGBO(51, 51, 51, 1))
                  )),
            ],
          ),
        )
    );
  }
}
