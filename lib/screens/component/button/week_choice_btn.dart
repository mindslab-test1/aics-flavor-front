import 'package:flutter/material.dart';
import 'package:heystars_app/providers/myinfo_provider.dart';
import 'package:provider/provider.dart';

class WeekChoiceBtn extends StatefulWidget {
  final String day;
  final int idx;

  const WeekChoiceBtn({Key key, this.day, this.idx}) : super(key: key);

  @override
  _WeekChoiceBtnState createState() => _WeekChoiceBtnState();
}

class _WeekChoiceBtnState extends State<WeekChoiceBtn> {
  MyInfoProvider myInfoProvider;
  bool check;

  @override
  Widget build(BuildContext context) {
    MyInfoProvider myInfoProvider = Provider.of<MyInfoProvider>(context);
    check = myInfoProvider.alarmDaysCheck[widget.idx];

    return Text(widget.day,
      style: TextStyle(
          fontSize: 10,
          fontFamily: 'NotoSansCJKkr',
          fontWeight: FontWeight.w400,
          color: check?Color.fromRGBO(39, 215, 148, 1):Color.fromRGBO(153, 153, 153, 1)
      ),);

  }

}
