import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SelectedChoiceBtn extends StatelessWidget {
  final String name;
  final String onImg;
  final String offImg;
  final int selectedIdx;
  final int idx;
  final Function changeIdx;
  final String category;

  SelectedChoiceBtn({Key key, this.name, this.onImg, this.offImg, this.selectedIdx, this.idx, this.changeIdx, this.category,}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return InkWell(
        onTap: (){
          changeIdx(idx);
        },
        child:Container(
          height: screenHeight * 0.063,
          child: Row(
            children: [
              category == 'Lang'?
              selectedIdx == idx
                  ?SvgPicture.asset(onImg)
                  :SvgPicture.asset(offImg)
              :selectedIdx ==idx
              ?Container(
                width: screenWidth * 0.077,
                height: screenWidth * 0.077,
                margin: EdgeInsets.only(left: screenWidth * 0.038),
                child: CircleAvatar(
                  backgroundImage: AssetImage(onImg),
                ),
              )
              :Container(
                width: screenWidth * 0.077,
                height: screenWidth * 0.077,
                margin: EdgeInsets.only(left: screenWidth * 0.038),
                child: CircleAvatar(
                  backgroundImage: AssetImage(offImg),
                ),
              ),
              SizedBox(
                width: screenWidth * 0.038,
              ),
              Text(name,
                style: TextStyle(
                    fontSize: 14,
                    fontFamily: 'NotoSansCJKkr',
                    fontWeight: FontWeight.w400,
                    color: selectedIdx == idx
                        ?Color.fromRGBO(39, 215, 148, 1)
                        :Color.fromRGBO(221, 221, 221, 1)
                ),),
              Expanded(child: Container()),
              Container(
                margin: EdgeInsets.only(right: screenWidth * 0.038),
                child: SvgPicture.asset(
                    selectedIdx == idx ?'assets/images/myinfo/check-24px.svg'
                        : 'assets/images/myinfo/uncheck-24px.svg'),
              )
            ],
          ),

        )
    );
  }
}
