import 'package:flutter/material.dart';

class StartBtn extends StatelessWidget {

  final Function function;

  StartBtn({
    this.function
  });

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      width: size.width*0.422,
      height: size.width*0.122,
      child: RaisedButton(
        elevation: 0,
        color: Color(0xff27d794),
        onPressed: function,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(24)
        ),
        child: Text(
          'Start',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontFamily: 'NotoSansCJKkr',
              fontWeight: FontWeight.w700
          ),
        ),
      ),
    );
  }
}
