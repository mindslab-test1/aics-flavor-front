import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:heystars_app/providers/user_provider.dart';
import 'package:heystars_app/screens/component/cards/chatbot/ai_avatar_chat.dart';
import 'package:heystars_app/services/common/font.dart';
import 'package:heystars_app/services/json_parser/chatbot/chatbot_json.dart';
import 'package:heystars_app/services/json_parser/chatbot/common_scenario_json.dart';
import 'package:provider/provider.dart';


class SelectAvatarAnswerBtn extends StatefulWidget {
  int selectedBtn;
  IntroScenario answer1;
  IntroScenario answer2;
  Function addChatList;
  Function studentChoice;
  Function playVideo;

  SelectAvatarAnswerBtn({
    this.selectedBtn,
    this.answer1,
    this.answer2,
    this.addChatList,
    this.studentChoice,
    this.playVideo
  });

  @override
  _SelectAvatarAnswerBtnState createState() => _SelectAvatarAnswerBtnState();
}

class _SelectAvatarAnswerBtnState extends State<SelectAvatarAnswerBtn> {
  //int selectedBtn;
  bool isSelected = false;
  UserProvider userProvider;
  
  @override
  void initState() {
    super.initState();
    userProvider = Provider.of(context, listen: false);  
  }

  
  String getOtherLangAnswer(ComScenarioModel comScenarioModel){
    if(userProvider.lang == 'Indo') return comScenarioModel.indoText;
    else if(userProvider.lang == 'Viet') return comScenarioModel.vietTExt;
    else return comScenarioModel.engText;
  }
  
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.only(bottom: 4, left: 24, right: 24),
      child: Container(
        constraints: BoxConstraints(minWidth: size.width*0.567,),
        decoration: BoxDecoration(
          color: Color(0xfff4f4ff),
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(30),
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30)
          )
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: (){
                setState(() {
                  if(widget.selectedBtn == null ) {
                    widget.selectedBtn = 1;
                    widget.addChatList(StudentAnswerWidget(widget.answer1.comModelAnswers[0].korText, getOtherLangAnswer(widget.answer1.comModelAnswers[0])));
                    Navigator.pop(context, true);
                  }
                });
              },
              child: Container(
                decoration: BoxDecoration(
                  color: (widget.selectedBtn==1)
                      ?Color(0xff27d794)
                      :Colors.transparent,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30)
                  )
                ),
                child: Padding(
                  padding: EdgeInsets.only(left: 24, right: 24, top: 16, bottom: 12),
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          widget.answer1.comModelAnswers[0].korText,
                          style: TextStyle(
                              fontFamily:korFont,
                              fontWeight: FontWeight.w500,
                              fontSize: 12,
                              color: (widget.selectedBtn==1)
                                  ?Colors.white
                                  :Color(0xff333333)
                          ),
                        ),
                        SizedBox(width: 8,),
                        Text(
                          getOtherLangAnswer(widget.answer1.comModelAnswers[0]),
                          style: TextStyle(
                              fontFamily: nkorFont,
                              fontWeight: FontWeight.w400,
                              fontSize: 12,
                              color: (widget.selectedBtn==1)
                                  ?Colors.white
                                  :Color(0xff333333)
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            (widget.answer2 == null)
                ?Container()
                :Divider(
                  height: 1,
                  thickness: 1,
                  color: Colors.white,
                ),
            (widget.answer2 == null)
                ?Container()
                :GestureDetector(
              onTap: (){
                setState(() {
                  if(widget.selectedBtn == null) {
                    widget.selectedBtn = 2;
                    widget.addChatList(StudentAnswerWidget(widget.answer2.comModelAnswers[0].korText, getOtherLangAnswer(widget.answer2.comModelAnswers[0])));

                    widget.playVideo(null, null, widget.answer2.comModelResponse.mediaModel.url, true);

                    widget.addChatList(AiChatMessage(null, null, getOtherLangAnswer(widget.answer2.comModelResponse), widget.answer2.comModelResponse.korText, null));
                  }
                });
              },

              child: Container(
                decoration: BoxDecoration(
                    color: (widget.selectedBtn==2)
                        ?Color(0xff27d794)
                        :Colors.transparent,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30)
                    )
                ),
                child: Padding(
                  padding: EdgeInsets.only(left: 24, right: 24, top: 12, bottom: 16),
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          widget.answer2.comModelAnswers[0].korText,
                          style: TextStyle(
                              fontFamily: korFont,
                              fontWeight: FontWeight.w500,
                              fontSize: 12,
                              color: (widget.selectedBtn==2)
                                  ?Colors.white
                                  :Color(0xff333333)
                          ),
                        ),
                        SizedBox(width: 8,),
                        Container(
                          constraints: BoxConstraints(maxWidth: 160),
                          child: Text(
                            getOtherLangAnswer(widget.answer2.comModelAnswers[0]),
                            style: TextStyle(
                                fontFamily: nkorFont,
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: (widget.selectedBtn==2)
                                    ?Colors.white
                                    :Color(0xff333333)
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
